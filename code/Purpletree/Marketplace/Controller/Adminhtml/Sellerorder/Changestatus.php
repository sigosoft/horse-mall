<?php
namespace   Purpletree\Marketplace\Controller\Adminhtml\Sellerorder;

use Magento\Backend\App\Action\Context;

class Changestatus extends \Magento\Backend\App\Action
{
    /**
     * @param \Magento\Backend\App\Action\Context $context
     */
    public function __construct(
        Context $context,
        \Purpletree\Marketplace\Model\ResourceModel\Sellerorder $sellerorder,
        \Magento\Sales\Model\Order $order,
        \Purpletree\Marketplace\Model\Commission $commissionModel,
        \Purpletree\Marketplace\Helper\Data $dataHelper,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Purpletree\Marketplace\Model\ResourceModel\Seller $storeDetails,
        \Purpletree\Marketplace\Model\CommissionFactory $commisssionmodeFactory,
        \Purpletree\Marketplace\Model\ResourceModel\Commission $commisssionmode,
        \Purpletree\Marketplace\Model\CategorycommissionFactory $commissionFactory,
        \Purpletree\Marketplace\Model\SellerorderFactory $sellerorderFactory
    ) {
        parent::__construct($context);
           $this->commissionModel           = $commissionModel;
           $this->_sellerorder           = $sellerorder;
           $this->_commisssionmode           = $commisssionmode;
           $this->_sellerorderFactory    = $sellerorderFactory;
           $this->dataHelper             = $dataHelper;
           $this->_commisssionmodeFactory           = $commisssionmodeFactory;
           $this->storeDetails           = $storeDetails;
           $this->productRepository      = $productRepository;
           $this->commissionFactory      = $commissionFactory;
           $this->order                  = $order;
    }

    public function execute()
    {
        $id                 = $this->getRequest()->getParam('id');
        $seller_id          = $this->getRequest()->getParam('seller_id');
        $order_id           = $this->getRequest()->getParam('order_id');
        $seller_status      = $this->getRequest()->getParam('seller_status');
        $entity_ids         = $this->_sellerorder->getEntityIdfromOrderId($seller_id, $order_id);
        $order              = $this->order->load($order_id);
        $order_increment_id     = $order->getIncrementId();
        $orderProducts      = $order->getAllVisibleItems();
        $commissionPercnt   = $this->dataHelper->getGeneralConfig('general/commission');
        foreach ($entity_ids as $idd) {
            $sellerorder = $this->_sellerorderFactory->create();
            $sellerorder->load($idd['entity_id']);
            $sellerorder->setOrderStatus($seller_status);
            $sellerorder->save();
            if ($this->dataHelper->getGeneralConfig('general/orderstatus') == $seller_status) {
                foreach ($orderProducts as $item) {
                    if ($sellerorder->getProductId() == $item->getProductId()) {
                    //
                        $product = $this->productRepository->getById($item->getProductId());
                        $sellerId = $product->getSellerId();
                        $final_cat_commison = '';
                        if ($sellerId!='') {
                                $productID = $item->getProductId();
                            // Category Commission
                            $catids = $product->getCategoryIds();
                                $commission_cat = [];
                                $catttt         = [];
                            if (!empty($catids)) {
                                $collection = $this->commissionFactory->create()->getCollection();
                                $collection->addFieldToFilter('category_id', $catids);
                                foreach ($collection as $dd) {
                                    $catttt[] = $dd->getCommission();
                                }
                            }
                            if (!empty($catttt)) {
                                $final_cat_commison = max($catttt);
                            }
                                $storeData = $this->getStoreDetails($sellerId);
                            if ($final_cat_commison != '') {
                                $commissionTotal = ($item->getPriceInclTax()*$final_cat_commison*$item->getQtyInvoiced())/100;
                            } // Category Commission
                            // Seller Commission
                            elseif ($storeData['store_commission'] != '') {
                                $commissionTotal = ($item->getPriceInclTax()*$storeData['store_commission']*$item->getQtyInvoiced())/100;
                            } // Seller Commission
                            // Config Global Commission
                            else {
                                $commissionTotal = ($item->getPriceInclTax()*$commissionPercnt*$item->getQtyOrdered())/100;
                            }
                            // Config Global Commission
                            $productPrice=$item->getPriceInclTax();
                            $productQuantity=$item->getQtyOrdered();
                            $productName=$product->getName();
                            $status = $seller_status;
                
                            if ($seller_status == 'complete') {
                                $commissionsave = $this->commissionModel;
                                $dddd = $this->_commisssionmode->getcommissionnnn($sellerId, $order_increment_id, $productID);
                                $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/tedddst.log');
                                $logger = new \Zend\Log\Logger();
                                $logger->addWriter($writer);
                                $logger->info('aaaa');
                                $logger->info($dddd);
                                if ($dddd && isset($dddd['entity_id']) && $dddd['entity_id'] != '') {
                                    $logger->info('bbb');
                                    $commissionsavea  = $this->_commisssionmodeFactory->create();
                                    $commissionsave  = $commissionsavea->load($dddd['entity_id']);
                                }
                                $commissionsave->setSellerId($sellerId);
                                $commissionsave->setOrderId($order_increment_id);
                                $commissionsave->setProductId($productID);
                                $commissionsave->setCommission($commissionTotal);
                                $commissionsave->setProductName($productName);
                                $commissionsave->setProductQuantity($productQuantity);
                                $commissionsave->setProductPrice($productPrice);
                                $commissionsave->setStatus($status);
                        
                                try {
                                    $commissionsave->save();
                                } catch (\Exception $e) {
                                    $this->messageManager->addException($e, __('Something went wrong while saving the details'));
                                }
                            }
                        }
                    }
                }
            }
        }
        $this->messageManager->addSuccess(__('Seller Order Status Updated Successfully.'));
        return $this->_redirect('purpletree_marketplace/sellerorder/view/', ['entity_id' => $id]);
    }
    public function getStoreDetails($sellerId)
    {
        return $this->storeDetails->getStoreDetails($sellerId);
    }
}
