<?php
namespace   Purpletree\Marketplace\Controller\Adminhtml\Sellerorder;

use Magento\Backend\App\Action\Context;

class View extends \Magento\Backend\App\Action
{
    /**
     * @param \Magento\Backend\App\Action\Context $context
     */
    public function __construct(
        Context $context
    ) {
        parent::__construct($context);
    }

    public function execute()
    {
        $id = $this->getRequest()->getParam('entity_id');
        $resultPage = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_PAGE);
        $resultPage->getConfig()->getTitle()->set(__('Seller Order'));
        return  $resultPage;
    }
}
