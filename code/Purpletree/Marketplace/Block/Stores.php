<?php

/**
 * Purpletree_Marketplace Seller
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Purpletree License that is bundled with this package in the file license.txt.
 * It is also available through online at this URL: https://www.purpletreesoftware.com/license.html
 *
 * @category    Purpletree
 * @package     Purpletree_Marketplace
 * @author      Purpletree Software
 * @copyright   Copyright (c) 2017
 * @license     https://www.purpletreesoftware.com/license.html
 */
 
namespace Purpletree\Marketplace\Block;

class Stores extends \Magento\Framework\View\Element\Template
{
    protected $sellers;
    
    /**
     * Constructor
     *
     * @param \Magento\Catalog\Model\Product\AttributeSet\Options
     * @param \Magento\Eav\Api\AttributeRepositoryInterface
     * @param \Purpletree\Marketplace\Model\ResourceModel\Seller
     * @param \Magento\Framework\Registry
     * @param \Magento\Store\Model\StoreManagerInterface
     * @param \Magento\Framework\View\Element\Template\Context
     * @param array $data
     */
    public function __construct(
        \Purpletree\Marketplace\Model\ResourceModel\Seller\CollectionFactory $sellersCollectionFactory,
        \Magento\Store\Model\StoreManager $storeManager,
        \Magento\Framework\App\ProductMetadataInterface $productMetadataInterface,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Framework\View\Element\Template\Context $context,
        array $data = []
    ) {
        $this->sellersCollectionFactory   = $sellersCollectionFactory;
        $this->storeManager               = $storeManager;
        $this->_productMetadataInterface  = $productMetadataInterface;
        $this->productCollectionFactory   = $productCollectionFactory;
        parent::__construct($context, $data);
    }
    public function getVersion()
    {
        return $this->_productMetadataInterface->getVersion();
    }
    public function getsellersearchpost()
    {
        $searchseller = '';
        if ($this->getRequest()->getParam('searchseller') && $this->getRequest()->getParam('searchseller') != '') {
            $searchseller = $this->getRequest()->getParam('searchseller');
        }
        return $searchseller;
    }
    public function getAllStores()
    {
        if (!$this->sellers) {
            if ($this->getRequest()->getParam('searchseller') && $this->getRequest()->getParam('searchseller') != '') {
                $this->sellers = $this->sellersCollectionFactory->create()
                           ->addFieldToSelect(
                               '*'
                           )->addFieldToFilter('store_name', ['like' => '%' . $this->getRequest()->getParam('searchseller'). '%'])
                                    ->addFieldToFilter(
                                        'status_id',
                                        '1'
                                    )->setOrder(
                                        'created_at',
                                        'desc'
                                    );
            } else {
                    $this->sellers = $this->sellersCollectionFactory->create()
                           ->addFieldToSelect(
                               '*'
                           )->addFieldToFilter(
                               'status_id',
                               '1'
                           )->setOrder(
                               'created_at',
                               'desc'
                           );
            }
        }
        return $this->sellers;
    }
        /**
         * @return $this
         */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if ($this->getAllStores()) {
            $pager = $this->getLayout()->createBlock(
                'Magento\Theme\Block\Html\Pager',
                'sales.order.history.pager'
            )->setCollection(
                $this->getAllStores()
            );
            $this->setChild('pager', $pager);
            $this->getAllStores()->load();
        }
        return $this;
    }
        /**
         * @return string
         */
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }
    public function getImageUrl()
    {
        return $this->storeManager->getStore()->getBaseUrl().'/pub/media/';
    }
    public function getSellerProducts($sellerId)
    {
            $this->productCollection = $this->productCollectionFactory->create()
                ->addAttributeToSelect('*')
                ->addAttributeToFilter('visibility', 4)
                ->addAttributeToFilter('status', 1)
                ->addAttributeToFilter('seller_id', $sellerId)
                ->load();
                            //Fix : Disabled product not coming in product collection in ver-Mage2.2.2
            $this->productCollection->clear();
            $fromAndJoin = $this->productCollection->getSelect()->getPart('FROM');
            $updatedfromAndJoin = [];
        foreach ($fromAndJoin as $key => $index) {
            if ($key == 'stock_status_index') {
                $index['joinType'] = 'left join';
            }
            $updatedfromAndJoin[$key] = $index;
        }
        if (!empty($updatedfromAndJoin)) {
            $this->productCollection->getSelect()->setPart('FROM', $updatedfromAndJoin);
        }

            $where = $this->productCollection->getSelect()->getPart('where');
            $updatedWhere = [];
        foreach ($where as $key => $condition) {
            if (version_compare($this->_productMetadataInterface->getVersion(), "2.3.0") == -1) {
                if (strpos($condition, 'stock_status_index.stock_status = 1') === false) {
                    $updatedWhere[] = $condition;
                }
            } else {
                if (strpos($condition, 'stock_status_index.is_salable = 1') === false) {
                    $updatedWhere[] = $condition;
                }
            }
        }
        if (!empty($updatedWhere)) {
            $this->productCollection->getSelect()->setPart('where', $updatedWhere);
        }
        return $this->productCollection;
    }
}
