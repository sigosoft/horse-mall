<?php
/**
 * Purpletree_Marketplace InstallSchema
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Purpletree License that is bundled with this package in the file license.txt.
 * It is also available through online at this URL: https://www.purpletreesoftware.com/license.html
 *
 * @category    Purpletree
 * @package     Purpletree_Marketplace
 * @author      Purpletree Software
 * @copyright   Copyright (c) 2017
 * @license     https://www.purpletreesoftware.com/license.html
 */
namespace Purpletree\Marketplace\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\ModuleContextInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * {@inheritdoc}
     */
    public function upgrade(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $installer = $setup;
        $installer->startSetup();
          $installer->getConnection()->addColumn(
              $installer->getTable('purpletree_marketplace_stores'),
              'store_commission',
              [
                    'type' => Table::TYPE_FLOAT,
                    'precision' => 10,
                    'scale' => 4,
                    'nullable' => true,
                    'default' => null,
                    'comment' => 'Store Commission'
                ]
          );
            // Create Category Commission Table
        $categoryCommissionTableName = $installer->getTable('purpletree_marketplace_categorycommission');
        // Check if the table already exists
        if ($installer->getConnection()->isTableExists($categoryCommissionTableName) != true) {
            $categoryCommissionTable = $installer->getConnection()
                ->newTable($categoryCommissionTableName)
                ->addColumn(
                    'entity_id',
                    Table::TYPE_INTEGER,
                    null,
                    ['identity' => true,'unsigned' => true, 'nullable' => false,'primary' => true],
                    'Entity ID'
                )
                ->addColumn(
                    'category_id',
                    Table::TYPE_INTEGER,
                    null,
                    ['unsigned' => true, 'nullable' => false],
                    'Category ID'
                )
                ->addColumn(
                    'commission',
                    Table::TYPE_DECIMAL,
                    null,
                    ['nullable' => false],
                    'Commission'
                )
                ->addColumn(
                    'updated_at',
                    Table::TYPE_DATETIME,
                    null,
                    ['nullable' => false],
                    'Updated At'
                )
                ->addColumn(
                    'created_at',
                    Table::TYPE_DATETIME,
                    null,
                    ['nullable' => false],
                    'Created At'
                )
                ->setComment('Purpletree Vendor Category Commission')
                ->setOption('type', 'InnoDB')
                ->setOption('charset', 'utf8');
                
            $installer->getConnection()->createTable($categoryCommissionTable);
        }
        // Create Category Commission Table
        // Create Seller Order Table
        $sellerOrderTableName = $installer->getTable('purpletree_marketplace_sellerorder');
        // Check if the table already exists
        if ($installer->getConnection()->isTableExists($sellerOrderTableName) != true) {
            $sellerOrderTable = $installer->getConnection()
                ->newTable($sellerOrderTableName)
                ->addColumn(
                    'entity_id',
                    Table::TYPE_INTEGER,
                    null,
                    ['identity' => true,'unsigned' => true, 'nullable' => false,'primary' => true],
                    'Entity ID'
                )
                ->addColumn(
                    'order_id',
                    Table::TYPE_INTEGER,
                    null,
                    ['unsigned' => true, 'nullable' => false],
                    'Order ID'
                )
                ->addColumn(
                    'seller_id',
                    Table::TYPE_INTEGER,
                    null,
                    ['unsigned' => true, 'nullable' => false],
                    'Seller ID'
                )
                ->addColumn(
                    'product_id',
                    Table::TYPE_INTEGER,
                    null,
                    ['unsigned' => true, 'nullable' => false],
                    'Product ID'
                )
                ->addColumn(
                    'order_status',
                    Table::TYPE_TEXT,
                    50,
                    ['unsigned' => true, 'nullable' => false],
                    'Order Status'
                )
                ->addColumn(
                    'updated_at',
                    Table::TYPE_DATETIME,
                    null,
                    ['nullable' => false],
                    'Updated At'
                )
                ->addColumn(
                    'created_at',
                    Table::TYPE_DATETIME,
                    null,
                    ['nullable' => false],
                    'Created At'
                )
                ->setComment('Purpletree Vendor Seller Order')
                ->setOption('type', 'InnoDB')
                ->setOption('charset', 'utf8');
                
            $installer->getConnection()->createTable($sellerOrderTable);
        }
        // Create Seller Order Table
        // Create Seller Order Invoice Table
        $sellerOrderInvoiceTableName = $installer->getTable('purpletree_marketplace_sellerorderinvoice');
        // Check if the table already exists
        if ($installer->getConnection()->isTableExists($sellerOrderInvoiceTableName) != true) {
            $sellerOrderInvoiceTable = $installer->getConnection()
                ->newTable($sellerOrderInvoiceTableName)
                ->addColumn(
                    'entity_id',
                    Table::TYPE_INTEGER,
                    null,
                    ['identity' => true,'unsigned' => true, 'nullable' => false,'primary' => true],
                    'Entity ID'
                )
                ->addColumn(
                    'order_id',
                    Table::TYPE_INTEGER,
                    null,
                    ['unsigned' => true, 'nullable' => false],
                    'Order ID'
                )
                ->addColumn(
                    'seller_id',
                    Table::TYPE_INTEGER,
                    null,
                    ['unsigned' => true, 'nullable' => false],
                    'Seller ID'
                )
				->addColumn(
                    'comment',
                    Table::TYPE_TEXT,
                    250,
                    ['unsigned' => true, 'nullable' => false],
                    'Comment'
                )
                ->addColumn(
                    'updated_at',
                    Table::TYPE_DATETIME,
                    null,
                    ['nullable' => false],
                    'Updated At'
                )
                ->addColumn(
                    'created_at',
                    Table::TYPE_DATETIME,
                    null,
                    ['nullable' => false],
                    'Created At'
                )
                ->setComment('Purpletree Vendor Seller Order Invoice')
                ->setOption('type', 'InnoDB')
                ->setOption('charset', 'utf8');
                
            $installer->getConnection()->createTable($sellerOrderInvoiceTable);
        } else {
			  $installer->getConnection()->addColumn(
              $installer->getTable('purpletree_marketplace_sellerorderinvoice'),
              'comment',
              [
                    'type' => Table::TYPE_TEXT,
					'length' => 250,
                    'nullable' => false,
                    'default' => null,
                    'comment' => 'Comment'
                ]
          );
		}
        // Create Seller Order Invoice Table
        $installer->endSetup();
    }
}
