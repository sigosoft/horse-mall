<?php

/**
 * Purpletree_Marketplace Data
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Purpletree License that is bundled with this package in the file license.txt.
 * It is also available through online at this URL: https://www.purpletreesoftware.com/license.html
 *
 * @category    Purpletree
 * @package     Purpletree_Marketplace
 * @author      Purpletree Software
 * @copyright   Copyright (c) 2017
 * @license     https://www.purpletreesoftware.com/license.html
 */

namespace Purpletree\Marketplace\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{

    const DEFAULT_ENABLED                  =   0;
    
    const XML_PATH_MARKETPLACE = 'purpletree_marketplace/';

    public function __construct(
        Context $context,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collectionFactory,
        \Magento\Framework\App\ProductMetadataInterface $productMetadataInterface,
        StoreManagerInterface $storeManager
    ) {
        $this->storeManager  = $storeManager;
        $this->transportBuilder     =       $transportBuilder;
        $this->inlineTranslation    =       $inlineTranslation;
         $this->_collectionFactory = $collectionFactory;
          $this->_productMetadataInterface             =       $productMetadataInterface;
        parent::__construct($context);
    }

    public function getConfigValue($field, $storeId = null)
    {
        return $this->scopeConfig->getValue(
            $field,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    public function getGeneralConfig($code, $storeId = null)
    {
        return $this->getConfigValue(self::XML_PATH_MARKETPLACE . $code, $storeId);
    }
    public function isEnabled($storeId = null)
    {
        $data = $this->getGeneralConfig('/general/enabled', $storeId);
        if ($data == null) {
            $data = self::DEFAULT_ENABLED;
        }
        return $data;
    }
    public function yourCustomMailSendMethod($emailTemplateVariables, $senderInfo, $receiverInfo, $identifier)
    {
        $this->temp_id = $identifier;
        $this->inlineTranslation->suspend();
        $this->generateTemplate($emailTemplateVariables, $senderInfo, $receiverInfo);
        $transport = $this->transportBuilder->getTransport();
        $transport->sendMessage();
        $this->inlineTranslation->resume();
    }
    public function generateTemplate($emailTemplateVariables, $senderInfo, $receiverInfo)
    {
        $template =  $this->transportBuilder->setTemplateIdentifier($this->temp_id)
              ->setTemplateOptions(
                  [
                      'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                      'store' => $this->storeManager->getStore()->getId(),
                  ]
              )
              ->setTemplateVars($emailTemplateVariables)
              ->setFrom($senderInfo)
              ->addTo($receiverInfo['email'], $receiverInfo['name']);
        return $this;
    }
    public function getSellerCollection($productId, $sellerId)
    {
        $collection = $this->_collectionFactory->create();
        $collection->addAttributeToSelect('product_id');
        $collection->addFieldToFilter('entity_id', $productId);
        $collection->addAttributeToFilter('seller_id', $sellerId);
        $collection->load();
                        //Fix : Disabled product not coming in product collection in ver-Mage2.2.2
        $collection->clear();
        $fromAndJoin = $collection->getSelect()->getPart('FROM');
        $updatedfromAndJoin = [];
        foreach ($fromAndJoin as $key => $index) {
            if ($key == 'stock_status_index') {
                $index['joinType'] = 'left join';
            }
            $updatedfromAndJoin[$key] = $index;
        }
        if (!empty($updatedfromAndJoin)) {
            $collection->getSelect()->setPart('FROM', $updatedfromAndJoin);
        }

        $where = $collection->getSelect()->getPart('where');
        $updatedWhere = [];
        foreach ($where as $key => $condition) {
            if (version_compare($this->_productMetadataInterface->getVersion(), "2.3.0") == -1) {
                if (strpos($condition, 'stock_status_index.stock_status = 1') === false) {
                    $updatedWhere[] = $condition;
                }
            } else {
                if (strpos($condition, 'stock_status_index.is_salable = 1') === false) {
                    $updatedWhere[] = $condition;
                }
            }
        }
        if (!empty($updatedWhere)) {
            $collection->getSelect()->setPart('where', $updatedWhere);
        }
        return $collection;
    }
}
