<?php
namespace Sigosoft\CartDisable\Controller\Index;

 
use Zend\Log\Filter\Timestamp;
 
class Post extends \Magento\Framework\App\Action\Action
{
    const XML_PATH_EMAIL_RECIPIENT_NAME = 'trans_email/ident_support/name';
    const XML_PATH_EMAIL_RECIPIENT_EMAIL = 'trans_email/ident_support/email';
     
    protected $_inlineTranslation;
    protected $_transportBuilder;
    protected $_scopeConfig;
    protected $_logLoggerInterface;
     
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Psr\Log\LoggerInterface $loggerInterface,
        array $data = []
         
        )
    {
        $this->_inlineTranslation = $inlineTranslation;
        $this->_transportBuilder = $transportBuilder;
        $this->_scopeConfig = $scopeConfig;
        $this->_logLoggerInterface = $loggerInterface;
        $this->messageManager = $context->getMessageManager();
         
         
        parent::__construct($context);
         
         
    }
     
    public function execute()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerSession = $objectManager->create('Magento\Customer\Model\Session'); 
        $this->_objectManager->get('Magento\Customer\Model\Session');
        if($customerSession->isLoggedIn()) {
            $post = $this->getRequest()->getPost();
            try
            {
                // Send Mail
                $this->_inlineTranslation->suspend();
                $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
                
                $sender = [
                    'url' => $post['url'],
                    'email' => $post['email'],
                    'name' => $post['name'],
                    'date' => $post['date']
                ];
                $sentToEmail = $this->_scopeConfig ->getValue('trans_email/ident_general/email',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                
                $sentToName = $this->_scopeConfig ->getValue('trans_email/ident_general/name',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                
                
                $transport = $this->_transportBuilder
                ->setTemplateIdentifier('cartdisable_email_template')
                ->setTemplateOptions(
                    [
                        'area' => 'frontend',
                        'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID,
                    ]
                    )
                    ->setTemplateVars([
                        'name'  => $post['name'],
                        'email'  => $post['email'],
                        'url' => $post['url'],
                        'date' => $post['date']
                    ])
                    ->setFrom($sender)
                    ->addTo($sentToEmail,$sentToName)
                    ->getTransport();
                    
                    $transport->sendMessage();
                    
                    $this->_inlineTranslation->resume();
                    $this->messageManager->addSuccess('Thank you sir. Your enquiry mail sent to our executive, They will contact you soon!!');
                    $resultRedirect = $this->resultRedirectFactory->create();
                    $resultRedirect->setRefererOrBaseUrl();
                    return $resultRedirect;
                    
            } catch(\Exception $e){
                $this->messageManager->addError($e->getMessage());
                $this->_logLoggerInterface->debug($e->getMessage());
                exit;
            }

        }
        else{
                $this->messageManager->addError(__("Please login first"));
                $this->_redirect('customer/account/login');
        }
        
         
         
         
    }
}