var config = {
    paths: {
        'bxslider': 'Sigosoft_Sliderad/js/jquery.bxslider'
    },
    shim: {
        'bxslider': {
            deps: ['jquery']
        }
    }
};