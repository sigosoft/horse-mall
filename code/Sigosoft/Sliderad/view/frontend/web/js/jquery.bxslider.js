/**
 * bxSlider v4.2.5
 * Copyright 2013-2015 Steven Wanderski
 * Written while drinking Belgian ales and listening to jazz

 * Licensed under MIT (http://opensource.org/licenses/MIT)
 */
define(['jquery'],function($){
(function() {

  var defaults = {

    // GENERAL
    mode: 'horizontal',
    slideSelector: '',
    infiniteLoop: true,
    hideControlOnEnd: false,
    speed: 500,
    easing: null,
    slideMargin: 0,
    startSlide: 0,
    randomStart: false,
    captions: false,
    ticker: false,
    tickerHover: false,
    adaptiveHeight: false,
    adaptiveHeightSpeed: 500,
    video: false,
    useCSS: true,
    preloadImages: 'visible',
    responsive: true,
    slideZIndex: 50,
    wrapperClass: 'bx-wrapper',

    // TOUCH
    touchEnabled: true,
    swipeThreshold: 50,
    oneToOneTouch: true,
    preventDefaultSwipeX: true,
    preventDefaultSwipeY: false,

    // ACCESSIBILITY
    ariaLive: true,
    ariaHidden: true,

    // KEYBOARD
    keyboardEnabled: false,

    // PAGER
    pager: true,
    pagerType: 'full',
    pagerShortSeparator: ' / ',
    pagerSelector: null,
    buildPager: null,
    pagerCustom: null,

    // CONTROLS
    controls: true,
    nextText: 'Next',
    prevText: 'Prev',
    nextSelector: null,
    prevSelector: null,
    autoControls: false,
    startText: 'Start',
    stopText: 'Stop',
    autoControlsCombine: false,
    autoControlsSelector: null,

    // AUTO
    auto: true,
    pause: 4000,
    autoStart: true,
    autoDirection: 'next',
    stopAutoOnClick: false,
    autoHover: false,
    autoDelay: 0,
    autoSlideForOnePage: false,

    // CAROUSEL
    minSlides: 1,
    maxSlides: 1,
    moveSlides: 0,
    slideWidth: 0,
    shrinkItems: false,

    // CALLBACKS
    onSlideradLoad: function() { return true; },
    onSlideBefore: function() { return true; },
    onSlideAfter: function() { return true; },
    onSlideNext: function() { return true; },
    onSlidePrev: function() { return true; },
    onSlideradResize: function() { return true; }
  };

  $.fn.bxSlider = function(options) {

    if (this.length === 0) {
      return this;
    }

    // support multiple elements
    if (this.length > 1) {
      this.each(function() {
        $(this).bxSlider(options);
      });
      return this;
    }

    // create a namespace to be used throughout the plugin
    var sliderad = {},
    // set a reference to our sliderad element
    el = this,
    // get the original window dimens (thanks a lot IE)
    windowWidth = $(window).width(),
    windowHeight = $(window).height();

    // Return if sliderad is already initialized
    if ($(el).data('bxSlider')) { return; }

    /**
     * ===================================================================================
     * = PRIVATE FUNCTIONS
     * ===================================================================================
     */

    /**
     * Initializes namespace settings to be used throughout plugin
     */
    var init = function() {
      // Return if sliderad is already initialized
      if ($(el).data('bxSlider')) { return; }
      // merge user-supplied options with the defaults
      sliderad.settings = $.extend({}, defaults, options);
      // parse slideWidth setting
      sliderad.settings.slideWidth = parseInt(sliderad.settings.slideWidth);
      // store the original children
      sliderad.children = el.children(sliderad.settings.slideSelector);
      // check if actual number of slides is less than minSlides / maxSlides
      if (sliderad.children.length < sliderad.settings.minSlides) { sliderad.settings.minSlides = sliderad.children.length; }
      if (sliderad.children.length < sliderad.settings.maxSlides) { sliderad.settings.maxSlides = sliderad.children.length; }
      // if random start, set the startSlide setting to random number
      if (sliderad.settings.randomStart) { sliderad.settings.startSlide = Math.floor(Math.random() * sliderad.children.length); }
      // store active slide information
      sliderad.active = { index: sliderad.settings.startSlide };
      // store if the sliderad is in carousel mode (displaying / moving multiple slides)
      sliderad.carousel = sliderad.settings.minSlides > 1 || sliderad.settings.maxSlides > 1 ? true : false;
      // if carousel, force preloadImages = 'all'
      if (sliderad.carousel) { sliderad.settings.preloadImages = 'all'; }
      // calculate the min / max width thresholds based on min / max number of slides
      // used to setup and update carousel slides dimensions
      sliderad.minThreshold = (sliderad.settings.minSlides * sliderad.settings.slideWidth) + ((sliderad.settings.minSlides - 1) * sliderad.settings.slideMargin);
      sliderad.maxThreshold = (sliderad.settings.maxSlides * sliderad.settings.slideWidth) + ((sliderad.settings.maxSlides - 1) * sliderad.settings.slideMargin);
      // store the current state of the sliderad (if currently animating, working is true)
      sliderad.working = false;
      // initialize the controls object
      sliderad.controls = {};
      // initialize an auto interval
      sliderad.interval = null;
      // determine which property to use for transitions
      sliderad.animProp = sliderad.settings.mode === 'vertical' ? 'top' : 'left';
      // determine if hardware acceleration can be used
      sliderad.usingCSS = sliderad.settings.useCSS && sliderad.settings.mode !== 'fade' && (function() {
        // create our test div element
        var div = document.createElement('div'),
        // css transition properties
        props = ['WebkitPerspective', 'MozPerspective', 'OPerspective', 'msPerspective'];
        // test for each property
        for (var i = 0; i < props.length; i++) {
          if (div.style[props[i]] !== undefined) {
            sliderad.cssPrefix = props[i].replace('Perspective', '').toLowerCase();
            sliderad.animProp = '-' + sliderad.cssPrefix + '-transform';
            return true;
          }
        }
        return false;
      }());
      // if vertical mode always make maxSlides and minSlides equal
      if (sliderad.settings.mode === 'vertical') { sliderad.settings.maxSlides = sliderad.settings.minSlides; }
      // save original style data
      el.data('origStyle', el.attr('style'));
      el.children(sliderad.settings.slideSelector).each(function() {
        $(this).data('origStyle', $(this).attr('style'));
      });

      // perform all DOM / CSS modifications
      setup();
    };

    /**
     * Performs all DOM and CSS modifications
     */
    var setup = function() {
      var preloadSelector = sliderad.children.eq(sliderad.settings.startSlide); // set the default preload selector (visible)

      // wrap el in a wrapper
      el.wrap('<div class="' + sliderad.settings.wrapperClass + '"><div class="bx-viewport"></div></div>');
      // store a namespace reference to .bx-viewport
      sliderad.viewport = el.parent();

      // add aria-live if the setting is enabled and ticker mode is disabled
      if (sliderad.settings.ariaLive && !sliderad.settings.ticker) {
        sliderad.viewport.attr('aria-live', 'polite');
      }
      // add a loading div to display while images are loading
      sliderad.loader = $('<div class="bx-loading" />');
      sliderad.viewport.prepend(sliderad.loader);
      // set el to a massive width, to hold any needed slides
      // also strip any margin and padding from el
      el.css({
        width: sliderad.settings.mode === 'horizontal' ? (sliderad.children.length * 1000 + 215) + '%' : 'auto',
        position: 'relative'
      });
      // if using CSS, add the easing property
      if (sliderad.usingCSS && sliderad.settings.easing) {
        el.css('-' + sliderad.cssPrefix + '-transition-timing-function', sliderad.settings.easing);
      // if not using CSS and no easing value was supplied, use the default JS animation easing (swing)
      } else if (!sliderad.settings.easing) {
        sliderad.settings.easing = 'swing';
      }
      // make modifications to the viewport (.bx-viewport)
      sliderad.viewport.css({
        width: '100%',
        overflow: 'hidden',
        position: 'relative'
      });
      sliderad.viewport.parent().css({
        maxWidth: getViewportMaxWidth()
      });
      // make modification to the wrapper (.bx-wrapper)
      if (!sliderad.settings.pager && !sliderad.settings.controls) {
        sliderad.viewport.parent().css({
          margin: '0 auto 0px'
        });
      }
      // apply css to all sliderad children
      sliderad.children.css({
        float: sliderad.settings.mode === 'horizontal' ? 'left' : 'none',
        listStyle: 'none',
        position: 'relative'
      });
      // apply the calculated width after the float is applied to prevent scrollbar interference
      sliderad.children.css('width', getSlideWidth());
      // if slideMargin is supplied, add the css
      if (sliderad.settings.mode === 'horizontal' && sliderad.settings.slideMargin > 0) { sliderad.children.css('marginRight', sliderad.settings.slideMargin); }
      if (sliderad.settings.mode === 'vertical' && sliderad.settings.slideMargin > 0) { sliderad.children.css('marginBottom', sliderad.settings.slideMargin); }
      // if "fade" mode, add positioning and z-index CSS
      if (sliderad.settings.mode === 'fade') {
        sliderad.children.css({
          position: 'absolute',
          zIndex: 0,
          display: 'none'
        });
        // prepare the z-index on the showing element
        sliderad.children.eq(sliderad.settings.startSlide).css({zIndex: sliderad.settings.slideZIndex, display: 'block'});
      }
      // create an element to contain all sliderad controls (pager, start / stop, etc)
      sliderad.controls.el = $('<div class="bx-controls" />');
      // if captions are requested, add them
      if (sliderad.settings.captions) { appendCaptions(); }
      // check if startSlide is last slide
      sliderad.active.last = sliderad.settings.startSlide === getPagerQty() - 1;
      // if video is true, set up the fitVids plugin
      if (sliderad.settings.video) { el.fitVids(); }
      if (sliderad.settings.preloadImages === 'all' || sliderad.settings.ticker) { preloadSelector = sliderad.children; }
      // only check for control addition if not in "ticker" mode
      if (!sliderad.settings.ticker) {
        // if controls are requested, add them
        if (sliderad.settings.controls) { appendControls(); }
        // if auto is true, and auto controls are requested, add them
        if (sliderad.settings.auto && sliderad.settings.autoControls) { appendControlsAuto(); }
        // if pager is requested, add it
        if (sliderad.settings.pager) { appendPager(); }
        // if any control option is requested, add the controls wrapper
        if (sliderad.settings.controls || sliderad.settings.autoControls || sliderad.settings.pager) { sliderad.viewport.after(sliderad.controls.el); }
      // if ticker mode, do not allow a pager
      } else {
        sliderad.settings.pager = false;
      }
      loadElements(preloadSelector, start);
    };

    var loadElements = function(selector, callback) {
      var total = selector.find('img:not([src=""]), iframe').length,
      count = 0;
      if (total === 0) {
        callback();
        return;
      }
      selector.find('img:not([src=""]), iframe').each(function() {
        $(this).one('load error', function() {
          if (++count === total) { callback(); }
        }).each(function() {
          if (this.complete) { $(this).load(); }
        });
      });
    };

    /**
     * Start the sliderad
     */
    var start = function() {
      // if infinite loop, prepare additional slides
      if (sliderad.settings.infiniteLoop && sliderad.settings.mode !== 'fade' && !sliderad.settings.ticker) {
        var slice    = sliderad.settings.mode === 'vertical' ? sliderad.settings.minSlides : sliderad.settings.maxSlides,
        sliceAppend  = sliderad.children.slice(0, slice).clone(true).addClass('bx-clone'),
        slicePrepend = sliderad.children.slice(-slice).clone(true).addClass('bx-clone');
        if (sliderad.settings.ariaHidden) {
          sliceAppend.attr('aria-hidden', true);
          slicePrepend.attr('aria-hidden', true);
        }
        el.append(sliceAppend).prepend(slicePrepend);
      }
      // remove the loading DOM element
      sliderad.loader.remove();
      // set the left / top position of "el"
      setSlidePosition();
      // if "vertical" mode, always use adaptiveHeight to prevent odd behavior
      if (sliderad.settings.mode === 'vertical') { sliderad.settings.adaptiveHeight = true; }
      // set the viewport height
      sliderad.viewport.height(getViewportHeight());
      // make sure everything is positioned just right (same as a window resize)
      el.redrawSliderad();
      // onSlideradLoad callback
      sliderad.settings.onSlideradLoad.call(el, sliderad.active.index);
      // sliderad has been fully initialized
      sliderad.initialized = true;
      // bind the resize call to the window
      if (sliderad.settings.responsive) { $(window).bind('resize', resizeWindow); }
      // if auto is true and has more than 1 page, start the show
      if (sliderad.settings.auto && sliderad.settings.autoStart && (getPagerQty() > 1 || sliderad.settings.autoSlideForOnePage)) { initAuto(); }
      // if ticker is true, start the ticker
      if (sliderad.settings.ticker) { initTicker(); }
      // if pager is requested, make the appropriate pager link active
      if (sliderad.settings.pager) { updatePagerActive(sliderad.settings.startSlide); }
      // check for any updates to the controls (like hideControlOnEnd updates)
      if (sliderad.settings.controls) { updateDirectionControls(); }
      // if touchEnabled is true, setup the touch events
      if (sliderad.settings.touchEnabled && !sliderad.settings.ticker) { initTouch(); }
      // if keyboardEnabled is true, setup the keyboard events
      if (sliderad.settings.keyboardEnabled && !sliderad.settings.ticker) {
        $(document).keydown(keyPress);
      }
    };

    /**
     * Returns the calculated height of the viewport, used to determine either adaptiveHeight or the maxHeight value
     */
    var getViewportHeight = function() {
      var height = 0;
      // first determine which children (slides) should be used in our height calculation
      var children = $();
      // if mode is not "vertical" and adaptiveHeight is false, include all children
      if (sliderad.settings.mode !== 'vertical' && !sliderad.settings.adaptiveHeight) {
        children = sliderad.children;
      } else {
        // if not carousel, return the single active child
        if (!sliderad.carousel) {
          children = sliderad.children.eq(sliderad.active.index);
        // if carousel, return a slice of children
        } else {
          // get the individual slide index
          var currentIndex = sliderad.settings.moveSlides === 1 ? sliderad.active.index : sliderad.active.index * getMoveBy();
          // add the current slide to the children
          children = sliderad.children.eq(currentIndex);
          // cycle through the remaining "showing" slides
          for (i = 1; i <= sliderad.settings.maxSlides - 1; i++) {
            // if looped back to the start
            if (currentIndex + i >= sliderad.children.length) {
              children = children.add(sliderad.children.eq(i - 1));
            } else {
              children = children.add(sliderad.children.eq(currentIndex + i));
            }
          }
        }
      }
      // if "vertical" mode, calculate the sum of the heights of the children
      if (sliderad.settings.mode === 'vertical') {
        children.each(function(index) {
          height += $(this).outerHeight();
        });
        // add user-supplied margins
        if (sliderad.settings.slideMargin > 0) {
          height += sliderad.settings.slideMargin * (sliderad.settings.minSlides - 1);
        }
      // if not "vertical" mode, calculate the max height of the children
      } else {
        height = Math.max.apply(Math, children.map(function() {
          return $(this).outerHeight(false);
        }).get());
      }

      if (sliderad.viewport.css('box-sizing') === 'border-box') {
        height += parseFloat(sliderad.viewport.css('padding-top')) + parseFloat(sliderad.viewport.css('padding-bottom')) +
              parseFloat(sliderad.viewport.css('border-top-width')) + parseFloat(sliderad.viewport.css('border-bottom-width'));
      } else if (sliderad.viewport.css('box-sizing') === 'padding-box') {
        height += parseFloat(sliderad.viewport.css('padding-top')) + parseFloat(sliderad.viewport.css('padding-bottom'));
      }

      return height;
    };

    /**
     * Returns the calculated width to be used for the outer wrapper / viewport
     */
    var getViewportMaxWidth = function() {
      var width = '100%';
      if (sliderad.settings.slideWidth > 0) {
        if (sliderad.settings.mode === 'horizontal') {
          width = (sliderad.settings.maxSlides * sliderad.settings.slideWidth) + ((sliderad.settings.maxSlides - 1) * sliderad.settings.slideMargin);
        } else {
          width = sliderad.settings.slideWidth;
        }
      }
      return width;
    };

    /**
     * Returns the calculated width to be applied to each slide
     */
    var getSlideWidth = function() {
      var newElWidth = sliderad.settings.slideWidth, // start with any user-supplied slide width
      wrapWidth      = sliderad.viewport.width();    // get the current viewport width
      // if slide width was not supplied, or is larger than the viewport use the viewport width
      if (sliderad.settings.slideWidth === 0 ||
        (sliderad.settings.slideWidth > wrapWidth && !sliderad.carousel) ||
        sliderad.settings.mode === 'vertical') {
        newElWidth = wrapWidth;
      // if carousel, use the thresholds to determine the width
      } else if (sliderad.settings.maxSlides > 1 && sliderad.settings.mode === 'horizontal') {
        if (wrapWidth > sliderad.maxThreshold) {
          return newElWidth;
        } else if (wrapWidth < sliderad.minThreshold) {
          newElWidth = (wrapWidth - (sliderad.settings.slideMargin * (sliderad.settings.minSlides - 1))) / sliderad.settings.minSlides;
        } else if (sliderad.settings.shrinkItems) {
          newElWidth = Math.floor((wrapWidth + sliderad.settings.slideMargin) / (Math.ceil((wrapWidth + sliderad.settings.slideMargin) / (newElWidth + sliderad.settings.slideMargin))) - sliderad.settings.slideMargin);
        }
      }
      return newElWidth;
    };

    /**
     * Returns the number of slides currently visible in the viewport (includes partially visible slides)
     */
    var getNumberSlidesShowing = function() {
      var slidesShowing = 1,
      childWidth = null;
      if (sliderad.settings.mode === 'horizontal' && sliderad.settings.slideWidth > 0) {
        // if viewport is smaller than minThreshold, return minSlides
        if (sliderad.viewport.width() < sliderad.minThreshold) {
          slidesShowing = sliderad.settings.minSlides;
        // if viewport is larger than maxThreshold, return maxSlides
        } else if (sliderad.viewport.width() > sliderad.maxThreshold) {
          slidesShowing = sliderad.settings.maxSlides;
        // if viewport is between min / max thresholds, divide viewport width by first child width
        } else {
          childWidth = sliderad.children.first().width() + sliderad.settings.slideMargin;
          slidesShowing = Math.floor((sliderad.viewport.width() +
            sliderad.settings.slideMargin) / childWidth);
        }
      // if "vertical" mode, slides showing will always be minSlides
      } else if (sliderad.settings.mode === 'vertical') {
        slidesShowing = sliderad.settings.minSlides;
      }
      return slidesShowing;
    };

    /**
     * Returns the number of pages (one full viewport of slides is one "page")
     */
    var getPagerQty = function() {
      var pagerQty = 0,
      breakPoint = 0,
      counter = 0;
      // if moveSlides is specified by the user
      if (sliderad.settings.moveSlides > 0) {
        if (sliderad.settings.infiniteLoop) {
          pagerQty = Math.ceil(sliderad.children.length / getMoveBy());
        } else {
          // when breakpoint goes above children length, counter is the number of pages
          while (breakPoint < sliderad.children.length) {
            ++pagerQty;
            breakPoint = counter + getNumberSlidesShowing();
            counter += sliderad.settings.moveSlides <= getNumberSlidesShowing() ? sliderad.settings.moveSlides : getNumberSlidesShowing();
          }
        }
      // if moveSlides is 0 (auto) divide children length by sides showing, then round up
      } else {
        pagerQty = Math.ceil(sliderad.children.length / getNumberSlidesShowing());
      }
      return pagerQty;
    };

    /**
     * Returns the number of individual slides by which to shift the sliderad
     */
    var getMoveBy = function() {
      // if moveSlides was set by the user and moveSlides is less than number of slides showing
      if (sliderad.settings.moveSlides > 0 && sliderad.settings.moveSlides <= getNumberSlidesShowing()) {
        return sliderad.settings.moveSlides;
      }
      // if moveSlides is 0 (auto)
      return getNumberSlidesShowing();
    };

    /**
     * Sets the sliderad's (el) left or top position
     */
    var setSlidePosition = function() {
      var position, lastChild, lastShowingIndex;
      // if last slide, not infinite loop, and number of children is larger than specified maxSlides
      if (sliderad.children.length > sliderad.settings.maxSlides && sliderad.active.last && !sliderad.settings.infiniteLoop) {
        if (sliderad.settings.mode === 'horizontal') {
          // get the last child's position
          lastChild = sliderad.children.last();
          position = lastChild.position();
          // set the left position
          setPositionProperty(-(position.left - (sliderad.viewport.width() - lastChild.outerWidth())), 'reset', 0);
        } else if (sliderad.settings.mode === 'vertical') {
          // get the last showing index's position
          lastShowingIndex = sliderad.children.length - sliderad.settings.minSlides;
          position = sliderad.children.eq(lastShowingIndex).position();
          // set the top position
          setPositionProperty(-position.top, 'reset', 0);
        }
      // if not last slide
      } else {
        // get the position of the first showing slide
        position = sliderad.children.eq(sliderad.active.index * getMoveBy()).position();
        // check for last slide
        if (sliderad.active.index === getPagerQty() - 1) { sliderad.active.last = true; }
        // set the respective position
        if (position !== undefined) {
          if (sliderad.settings.mode === 'horizontal') { setPositionProperty(-position.left, 'reset', 0); }
          else if (sliderad.settings.mode === 'vertical') { setPositionProperty(-position.top, 'reset', 0); }
        }
      }
    };

    /**
     * Sets the el's animating property position (which in turn will sometimes animate el).
     * If using CSS, sets the transform property. If not using CSS, sets the top / left property.
     *
     * @param value (int)
     *  - the animating property's value
     *
     * @param type (string) 'slide', 'reset', 'ticker'
     *  - the type of instance for which the function is being
     *
     * @param duration (int)
     *  - the amount of time (in ms) the transition should occupy
     *
     * @param params (array) optional
     *  - an optional parameter containing any variables that need to be passed in
     */
    var setPositionProperty = function(value, type, duration, params) {
      var animateObj, propValue;
      // use CSS transform
      if (sliderad.usingCSS) {
        // determine the translate3d value
        propValue = sliderad.settings.mode === 'vertical' ? 'translate3d(0, ' + value + 'px, 0)' : 'translate3d(' + value + 'px, 0, 0)';
        // add the CSS transition-duration
        el.css('-' + sliderad.cssPrefix + '-transition-duration', duration / 1000 + 's');
        if (type === 'slide') {
          // set the property value
          el.css(sliderad.animProp, propValue);
          if (duration !== 0) {
            // bind a callback method - executes when CSS transition completes
            el.bind('transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd', function(e) {
              //make sure it's the correct one
              if (!$(e.target).is(el)) { return; }
              // unbind the callback
              el.unbind('transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd');
              updateAfterSlideTransition();
            });
          } else { //duration = 0
            updateAfterSlideTransition();
          }
        } else if (type === 'reset') {
          el.css(sliderad.animProp, propValue);
        } else if (type === 'ticker') {
          // make the transition use 'linear'
          el.css('-' + sliderad.cssPrefix + '-transition-timing-function', 'linear');
          el.css(sliderad.animProp, propValue);
          if (duration !== 0) {
            el.bind('transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd', function(e) {
              //make sure it's the correct one
              if (!$(e.target).is(el)) { return; }
              // unbind the callback
              el.unbind('transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd');
              // reset the position
              setPositionProperty(params.resetValue, 'reset', 0);
              // start the loop again
              tickerLoop();
            });
          } else { //duration = 0
            setPositionProperty(params.resetValue, 'reset', 0);
            tickerLoop();
          }
        }
      // use JS animate
      } else {
        animateObj = {};
        animateObj[sliderad.animProp] = value;
        if (type === 'slide') {
          el.animate(animateObj, duration, sliderad.settings.easing, function() {
            updateAfterSlideTransition();
          });
        } else if (type === 'reset') {
          el.css(sliderad.animProp, value);
        } else if (type === 'ticker') {
          el.animate(animateObj, duration, 'linear', function() {
            setPositionProperty(params.resetValue, 'reset', 0);
            // run the recursive loop after animation
            tickerLoop();
          });
        }
      }
    };

    /**
     * Populates the pager with proper amount of pages
     */
    var populatePager = function() {
      var pagerHtml = '',
      linkContent = '',
      pagerQty = getPagerQty();
      // loop through each pager item
      for (var i = 0; i < pagerQty; i++) {
        linkContent = '';
        // if a buildPager function is supplied, use it to get pager link value, else use index + 1
        if (sliderad.settings.buildPager && $.isFunction(sliderad.settings.buildPager) || sliderad.settings.pagerCustom) {
          linkContent = sliderad.settings.buildPager(i);
          sliderad.pagerEl.addClass('bx-custom-pager');
        } else {
          linkContent = i + 1;
          sliderad.pagerEl.addClass('bx-default-pager');
        }
        // var linkContent = sliderad.settings.buildPager && $.isFunction(sliderad.settings.buildPager) ? sliderad.settings.buildPager(i) : i + 1;
        // add the markup to the string
        pagerHtml += '<div class="bx-pager-item"><a href="" data-slide-index="' + i + '" class="bx-pager-link">' + linkContent + '</a></div>';
      }
      // populate the pager element with pager links
      sliderad.pagerEl.html(pagerHtml);
    };

    /**
     * Appends the pager to the controls element
     */
    var appendPager = function() {
      if (!sliderad.settings.pagerCustom) {
        // create the pager DOM element
        sliderad.pagerEl = $('<div class="bx-pager" />');
        // if a pager selector was supplied, populate it with the pager
        if (sliderad.settings.pagerSelector) {
          $(sliderad.settings.pagerSelector).html(sliderad.pagerEl);
        // if no pager selector was supplied, add it after the wrapper
        } else {
          sliderad.controls.el.addClass('bx-has-pager').append(sliderad.pagerEl);
        }
        // populate the pager
        populatePager();
      } else {
        sliderad.pagerEl = $(sliderad.settings.pagerCustom);
      }
      // assign the pager click binding
      sliderad.pagerEl.on('click touchend', 'a', clickPagerBind);
    };

    /**
     * Appends prev / next controls to the controls element
     */
    var appendControls = function() {
      sliderad.controls.next = $('<a class="bx-next" href="">' + sliderad.settings.nextText + '</a>');
      sliderad.controls.prev = $('<a class="bx-prev" href="">' + sliderad.settings.prevText + '</a>');
      // bind click actions to the controls
      sliderad.controls.next.bind('click touchend', clickNextBind);
      sliderad.controls.prev.bind('click touchend', clickPrevBind);
      // if nextSelector was supplied, populate it
      if (sliderad.settings.nextSelector) {
        $(sliderad.settings.nextSelector).append(sliderad.controls.next);
      }
      // if prevSelector was supplied, populate it
      if (sliderad.settings.prevSelector) {
        $(sliderad.settings.prevSelector).append(sliderad.controls.prev);
      }
      // if no custom selectors were supplied
      if (!sliderad.settings.nextSelector && !sliderad.settings.prevSelector) {
        // add the controls to the DOM
        sliderad.controls.directionEl = $('<div class="bx-controls-direction" />');
        // add the control elements to the directionEl
        sliderad.controls.directionEl.append(sliderad.controls.prev).append(sliderad.controls.next);
        // sliderad.viewport.append(sliderad.controls.directionEl);
        sliderad.controls.el.addClass('bx-has-controls-direction').append(sliderad.controls.directionEl);
      }
    };

    /**
     * Appends start / stop auto controls to the controls element
     */
    var appendControlsAuto = function() {
      sliderad.controls.start = $('<div class="bx-controls-auto-item"><a class="bx-start" href="">' + sliderad.settings.startText + '</a></div>');
      sliderad.controls.stop = $('<div class="bx-controls-auto-item"><a class="bx-stop" href="">' + sliderad.settings.stopText + '</a></div>');
      // add the controls to the DOM
      sliderad.controls.autoEl = $('<div class="bx-controls-auto" />');
      // bind click actions to the controls
      sliderad.controls.autoEl.on('click', '.bx-start', clickStartBind);
      sliderad.controls.autoEl.on('click', '.bx-stop', clickStopBind);
      // if autoControlsCombine, insert only the "start" control
      if (sliderad.settings.autoControlsCombine) {
        sliderad.controls.autoEl.append(sliderad.controls.start);
      // if autoControlsCombine is false, insert both controls
      } else {
        sliderad.controls.autoEl.append(sliderad.controls.start).append(sliderad.controls.stop);
      }
      // if auto controls selector was supplied, populate it with the controls
      if (sliderad.settings.autoControlsSelector) {
        $(sliderad.settings.autoControlsSelector).html(sliderad.controls.autoEl);
      // if auto controls selector was not supplied, add it after the wrapper
      } else {
        sliderad.controls.el.addClass('bx-has-controls-auto').append(sliderad.controls.autoEl);
      }
      // update the auto controls
      updateAutoControls(sliderad.settings.autoStart ? 'stop' : 'start');
    };

    /**
     * Appends image captions to the DOM
     */
    var appendCaptions = function() {
      // cycle through each child
      sliderad.children.each(function(index) {
        // get the image title attribute
        var title = $(this).find('img:first').attr('title');
        // append the caption
        if (title !== undefined && ('' + title).length) {
          $(this).append('<div class="bx-caption"><span>' + title + '</span></div>');
        }
      });
    };

    /**
     * Click next binding
     *
     * @param e (event)
     *  - DOM event object
     */
    var clickNextBind = function(e) {
      e.preventDefault();
      if (sliderad.controls.el.hasClass('disabled')) { return; }
      // if auto show is running, stop it
      if (sliderad.settings.auto && sliderad.settings.stopAutoOnClick) { el.stopAuto(); }
      el.goToNextSlide();
    };

    /**
     * Click prev binding
     *
     * @param e (event)
     *  - DOM event object
     */
    var clickPrevBind = function(e) {
      e.preventDefault();
      if (sliderad.controls.el.hasClass('disabled')) { return; }
      // if auto show is running, stop it
      if (sliderad.settings.auto && sliderad.settings.stopAutoOnClick) { el.stopAuto(); }
      el.goToPrevSlide();
    };

    /**
     * Click start binding
     *
     * @param e (event)
     *  - DOM event object
     */
    var clickStartBind = function(e) {
      el.startAuto();
      e.preventDefault();
    };

    /**
     * Click stop binding
     *
     * @param e (event)
     *  - DOM event object
     */
    var clickStopBind = function(e) {
      el.stopAuto();
      e.preventDefault();
    };

    /**
     * Click pager binding
     *
     * @param e (event)
     *  - DOM event object
     */
    var clickPagerBind = function(e) {
      var pagerLink, pagerIndex;
      e.preventDefault();
      if (sliderad.controls.el.hasClass('disabled')) {
        return;
      }
      // if auto show is running, stop it
      if (sliderad.settings.auto  && sliderad.settings.stopAutoOnClick) { el.stopAuto(); }
      pagerLink = $(e.currentTarget);
      if (pagerLink.attr('data-slide-index') !== undefined) {
        pagerIndex = parseInt(pagerLink.attr('data-slide-index'));
        // if clicked pager link is not active, continue with the goToSlide call
        if (pagerIndex !== sliderad.active.index) { el.goToSlide(pagerIndex); }
      }
    };

    /**
     * Updates the pager links with an active class
     *
     * @param slideIndex (int)
     *  - index of slide to make active
     */
    var updatePagerActive = function(slideIndex) {
      // if "short" pager type
      var len = sliderad.children.length; // nb of children
      if (sliderad.settings.pagerType === 'short') {
        if (sliderad.settings.maxSlides > 1) {
          len = Math.ceil(sliderad.children.length / sliderad.settings.maxSlides);
        }
        sliderad.pagerEl.html((slideIndex + 1) + sliderad.settings.pagerShortSeparator + len);
        return;
      }
      // remove all pager active classes
      sliderad.pagerEl.find('a').removeClass('active');
      // apply the active class for all pagers
      sliderad.pagerEl.each(function(i, el) { $(el).find('a').eq(slideIndex).addClass('active'); });
    };

    /**
     * Performs needed actions after a slide transition
     */
    var updateAfterSlideTransition = function() {
      // if infinite loop is true
      if (sliderad.settings.infiniteLoop) {
        var position = '';
        // first slide
        if (sliderad.active.index === 0) {
          // set the new position
          position = sliderad.children.eq(0).position();
        // carousel, last slide
        } else if (sliderad.active.index === getPagerQty() - 1 && sliderad.carousel) {
          position = sliderad.children.eq((getPagerQty() - 1) * getMoveBy()).position();
        // last slide
        } else if (sliderad.active.index === sliderad.children.length - 1) {
          position = sliderad.children.eq(sliderad.children.length - 1).position();
        }
        if (position) {
          if (sliderad.settings.mode === 'horizontal') { setPositionProperty(-position.left, 'reset', 0); }
          else if (sliderad.settings.mode === 'vertical') { setPositionProperty(-position.top, 'reset', 0); }
        }
      }
      // declare that the transition is complete
      sliderad.working = false;
      // onSlideAfter callback
      sliderad.settings.onSlideAfter.call(el, sliderad.children.eq(sliderad.active.index), sliderad.oldIndex, sliderad.active.index);
    };

    /**
     * Updates the auto controls state (either active, or combined switch)
     *
     * @param state (string) "start", "stop"
     *  - the new state of the auto show
     */
    var updateAutoControls = function(state) {
      // if autoControlsCombine is true, replace the current control with the new state
      if (sliderad.settings.autoControlsCombine) {
        sliderad.controls.autoEl.html(sliderad.controls[state]);
      // if autoControlsCombine is false, apply the "active" class to the appropriate control
      } else {
        sliderad.controls.autoEl.find('a').removeClass('active');
        sliderad.controls.autoEl.find('a:not(.bx-' + state + ')').addClass('active');
      }
    };

    /**
     * Updates the direction controls (checks if either should be hidden)
     */
    var updateDirectionControls = function() {
      if (getPagerQty() === 1) {
        sliderad.controls.prev.addClass('disabled');
        sliderad.controls.next.addClass('disabled');
      } else if (!sliderad.settings.infiniteLoop && sliderad.settings.hideControlOnEnd) {
        // if first slide
        if (sliderad.active.index === 0) {
          sliderad.controls.prev.addClass('disabled');
          sliderad.controls.next.removeClass('disabled');
        // if last slide
        } else if (sliderad.active.index === getPagerQty() - 1) {
          sliderad.controls.next.addClass('disabled');
          sliderad.controls.prev.removeClass('disabled');
        // if any slide in the middle
        } else {
          sliderad.controls.prev.removeClass('disabled');
          sliderad.controls.next.removeClass('disabled');
        }
      }
    };

    /**
     * Initializes the auto process
     */
    var initAuto = function() {
      // if autoDelay was supplied, launch the auto show using a setTimeout() call
      if (sliderad.settings.autoDelay > 0) {
        var timeout = setTimeout(el.startAuto, sliderad.settings.autoDelay);
      // if autoDelay was not supplied, start the auto show normally
      } else {
        el.startAuto();

        //add focus and blur events to ensure its running if timeout gets paused
        $(window).focus(function() {
          el.startAuto();
        }).blur(function() {
          el.stopAuto();
        });
      }
      // if autoHover is requested
      if (sliderad.settings.autoHover) {
        // on el hover
        el.hover(function() {
          // if the auto show is currently playing (has an active interval)
          if (sliderad.interval) {
            // stop the auto show and pass true argument which will prevent control update
            el.stopAuto(true);
            // create a new autoPaused value which will be used by the relative "mouseout" event
            sliderad.autoPaused = true;
          }
        }, function() {
          // if the autoPaused value was created be the prior "mouseover" event
          if (sliderad.autoPaused) {
            // start the auto show and pass true argument which will prevent control update
            el.startAuto(true);
            // reset the autoPaused value
            sliderad.autoPaused = null;
          }
        });
      }
    };

    /**
     * Initializes the ticker process
     */
    var initTicker = function() {
      var startPosition = 0,
      position, transform, value, idx, ratio, property, newSpeed, totalDimens;
      // if autoDirection is "next", append a clone of the entire sliderad
      if (sliderad.settings.autoDirection === 'next') {
        el.append(sliderad.children.clone().addClass('bx-clone'));
      // if autoDirection is "prev", prepend a clone of the entire sliderad, and set the left position
      } else {
        el.prepend(sliderad.children.clone().addClass('bx-clone'));
        position = sliderad.children.first().position();
        startPosition = sliderad.settings.mode === 'horizontal' ? -position.left : -position.top;
      }
      setPositionProperty(startPosition, 'reset', 0);
      // do not allow controls in ticker mode
      sliderad.settings.pager = false;
      sliderad.settings.controls = false;
      sliderad.settings.autoControls = false;
      // if autoHover is requested
      if (sliderad.settings.tickerHover) {
        if (sliderad.usingCSS) {
          idx = sliderad.settings.mode === 'horizontal' ? 4 : 5;
          sliderad.viewport.hover(function() {
            transform = el.css('-' + sliderad.cssPrefix + '-transform');
            value = parseFloat(transform.split(',')[idx]);
            setPositionProperty(value, 'reset', 0);
          }, function() {
            totalDimens = 0;
            sliderad.children.each(function(index) {
              totalDimens += sliderad.settings.mode === 'horizontal' ? $(this).outerWidth(true) : $(this).outerHeight(true);
            });
            // calculate the speed ratio (used to determine the new speed to finish the paused animation)
            ratio = sliderad.settings.speed / totalDimens;
            // determine which property to use
            property = sliderad.settings.mode === 'horizontal' ? 'left' : 'top';
            // calculate the new speed
            newSpeed = ratio * (totalDimens - (Math.abs(parseInt(value))));
            tickerLoop(newSpeed);
          });
        } else {
          // on el hover
          sliderad.viewport.hover(function() {
            el.stop();
          }, function() {
            // calculate the total width of children (used to calculate the speed ratio)
            totalDimens = 0;
            sliderad.children.each(function(index) {
              totalDimens += sliderad.settings.mode === 'horizontal' ? $(this).outerWidth(true) : $(this).outerHeight(true);
            });
            // calculate the speed ratio (used to determine the new speed to finish the paused animation)
            ratio = sliderad.settings.speed / totalDimens;
            // determine which property to use
            property = sliderad.settings.mode === 'horizontal' ? 'left' : 'top';
            // calculate the new speed
            newSpeed = ratio * (totalDimens - (Math.abs(parseInt(el.css(property)))));
            tickerLoop(newSpeed);
          });
        }
      }
      // start the ticker loop
      tickerLoop();
    };

    /**
     * Runs a continuous loop, news ticker-style
     */
    var tickerLoop = function(resumeSpeed) {
      var speed = resumeSpeed ? resumeSpeed : sliderad.settings.speed,
      position = {left: 0, top: 0},
      reset = {left: 0, top: 0},
      animateProperty, resetValue, params;

      // if "next" animate left position to last child, then reset left to 0
      if (sliderad.settings.autoDirection === 'next') {
        position = el.find('.bx-clone').first().position();
      // if "prev" animate left position to 0, then reset left to first non-clone child
      } else {
        reset = sliderad.children.first().position();
      }
      animateProperty = sliderad.settings.mode === 'horizontal' ? -position.left : -position.top;
      resetValue = sliderad.settings.mode === 'horizontal' ? -reset.left : -reset.top;
      params = {resetValue: resetValue};
      setPositionProperty(animateProperty, 'ticker', speed, params);
    };

    /**
     * Check if el is on screen
     */
    var isOnScreen = function(el) {
      var win = $(window),
      viewport = {
        top: win.scrollTop(),
        left: win.scrollLeft()
      },
      bounds = el.offset();

      viewport.right = viewport.left + win.width();
      viewport.bottom = viewport.top + win.height();
      bounds.right = bounds.left + el.outerWidth();
      bounds.bottom = bounds.top + el.outerHeight();

      return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));
    };

    /**
     * Initializes keyboard events
     */
    var keyPress = function(e) {
      var activeElementTag = document.activeElement.tagName.toLowerCase(),
      tagFilters = 'input|textarea',
      p = new RegExp(activeElementTag,['i']),
      result = p.exec(tagFilters);

      if (result == null && isOnScreen(el)) {
        if (e.keyCode === 39) {
          clickNextBind(e);
          return false;
        } else if (e.keyCode === 37) {
          clickPrevBind(e);
          return false;
        }
      }
    };

    /**
     * Initializes touch events
     */
    var initTouch = function() {
      // initialize object to contain all touch values
      sliderad.touch = {
        start: {x: 0, y: 0},
        end: {x: 0, y: 0}
      };
      sliderad.viewport.bind('touchstart MSPointerDown pointerdown', onTouchStart);

      //for browsers that have implemented pointer events and fire a click after
      //every pointerup regardless of whether pointerup is on same screen location as pointerdown or not
      sliderad.viewport.on('click', '.bxslider a', function(e) {
        if (sliderad.viewport.hasClass('click-disabled')) {
          e.preventDefault();
          sliderad.viewport.removeClass('click-disabled');
        }
      });
    };

    /**
     * Event handler for "touchstart"
     *
     * @param e (event)
     *  - DOM event object
     */
    var onTouchStart = function(e) {
      //disable sliderad controls while user is interacting with slides to avoid sliderad freeze that happens on touch devices when a slide swipe happens immediately after interacting with sliderad controls
      sliderad.controls.el.addClass('disabled');

      if (sliderad.working) {
        e.preventDefault();
        sliderad.controls.el.removeClass('disabled');
      } else {
        // record the original position when touch starts
        sliderad.touch.originalPos = el.position();
        var orig = e.originalEvent,
        touchPoints = (typeof orig.changedTouches !== 'undefined') ? orig.changedTouches : [orig];
        // record the starting touch x, y coordinates
        sliderad.touch.start.x = touchPoints[0].pageX;
        sliderad.touch.start.y = touchPoints[0].pageY;

        if (sliderad.viewport.get(0).setPointerCapture) {
          sliderad.pointerId = orig.pointerId;
          sliderad.viewport.get(0).setPointerCapture(sliderad.pointerId);
        }
        // bind a "touchmove" event to the viewport
        sliderad.viewport.bind('touchmove MSPointerMove pointermove', onTouchMove);
        // bind a "touchend" event to the viewport
        sliderad.viewport.bind('touchend MSPointerUp pointerup', onTouchEnd);
        sliderad.viewport.bind('MSPointerCancel pointercancel', onPointerCancel);
      }
    };

    /**
     * Cancel Pointer for Windows Phone
     *
     * @param e (event)
     *  - DOM event object
     */
    var onPointerCancel = function(e) {
      /* onPointerCancel handler is needed to deal with situations when a touchend
      doesn't fire after a touchstart (this happens on windows phones only) */
      setPositionProperty(sliderad.touch.originalPos.left, 'reset', 0);

      //remove handlers
      sliderad.controls.el.removeClass('disabled');
      sliderad.viewport.unbind('MSPointerCancel pointercancel', onPointerCancel);
      sliderad.viewport.unbind('touchmove MSPointerMove pointermove', onTouchMove);
      sliderad.viewport.unbind('touchend MSPointerUp pointerup', onTouchEnd);
      if (sliderad.viewport.get(0).releasePointerCapture) {
        sliderad.viewport.get(0).releasePointerCapture(sliderad.pointerId);
      }
    };

    /**
     * Event handler for "touchmove"
     *
     * @param e (event)
     *  - DOM event object
     */
    var onTouchMove = function(e) {
      var orig = e.originalEvent,
      touchPoints = (typeof orig.changedTouches !== 'undefined') ? orig.changedTouches : [orig],
      // if scrolling on y axis, do not prevent default
      xMovement = Math.abs(touchPoints[0].pageX - sliderad.touch.start.x),
      yMovement = Math.abs(touchPoints[0].pageY - sliderad.touch.start.y),
      value = 0,
      change = 0;

      // x axis swipe
      if ((xMovement * 3) > yMovement && sliderad.settings.preventDefaultSwipeX) {
        e.preventDefault();
      // y axis swipe
      } else if ((yMovement * 3) > xMovement && sliderad.settings.preventDefaultSwipeY) {
        e.preventDefault();
      }
      if (sliderad.settings.mode !== 'fade' && sliderad.settings.oneToOneTouch) {
        // if horizontal, drag along x axis
        if (sliderad.settings.mode === 'horizontal') {
          change = touchPoints[0].pageX - sliderad.touch.start.x;
          value = sliderad.touch.originalPos.left + change;
        // if vertical, drag along y axis
        } else {
          change = touchPoints[0].pageY - sliderad.touch.start.y;
          value = sliderad.touch.originalPos.top + change;
        }
        setPositionProperty(value, 'reset', 0);
      }
    };

    /**
     * Event handler for "touchend"
     *
     * @param e (event)
     *  - DOM event object
     */
    var onTouchEnd = function(e) {
      sliderad.viewport.unbind('touchmove MSPointerMove pointermove', onTouchMove);
      //enable sliderad controls as soon as user stops interacing with slides
      sliderad.controls.el.removeClass('disabled');
      var orig    = e.originalEvent,
      touchPoints = (typeof orig.changedTouches !== 'undefined') ? orig.changedTouches : [orig],
      value       = 0,
      distance    = 0;
      // record end x, y positions
      sliderad.touch.end.x = touchPoints[0].pageX;
      sliderad.touch.end.y = touchPoints[0].pageY;
      // if fade mode, check if absolute x distance clears the threshold
      if (sliderad.settings.mode === 'fade') {
        distance = Math.abs(sliderad.touch.start.x - sliderad.touch.end.x);
        if (distance >= sliderad.settings.swipeThreshold) {
          if (sliderad.touch.start.x > sliderad.touch.end.x) {
            el.goToNextSlide();
          } else {
            el.goToPrevSlide();
          }
          el.stopAuto();
        }
      // not fade mode
      } else {
        // calculate distance and el's animate property
        if (sliderad.settings.mode === 'horizontal') {
          distance = sliderad.touch.end.x - sliderad.touch.start.x;
          value = sliderad.touch.originalPos.left;
        } else {
          distance = sliderad.touch.end.y - sliderad.touch.start.y;
          value = sliderad.touch.originalPos.top;
        }
        // if not infinite loop and first / last slide, do not attempt a slide transition
        if (!sliderad.settings.infiniteLoop && ((sliderad.active.index === 0 && distance > 0) || (sliderad.active.last && distance < 0))) {
          setPositionProperty(value, 'reset', 200);
        } else {
          // check if distance clears threshold
          if (Math.abs(distance) >= sliderad.settings.swipeThreshold) {
            if (distance < 0) {
              el.goToNextSlide();
            } else {
              el.goToPrevSlide();
            }
            el.stopAuto();
          } else {
            // el.animate(property, 200);
            setPositionProperty(value, 'reset', 200);
          }
        }
      }
      sliderad.viewport.unbind('touchend MSPointerUp pointerup', onTouchEnd);
      if (sliderad.viewport.get(0).releasePointerCapture) {
        sliderad.viewport.get(0).releasePointerCapture(sliderad.pointerId);
      }
    };

    /**
     * Window resize event callback
     */
    var resizeWindow = function(e) {
      // don't do anything if sliderad isn't initialized.
      if (!sliderad.initialized) { return; }
      // Delay if sliderad working.
      if (sliderad.working) {
        window.setTimeout(resizeWindow, 10);
      } else {
        // get the new window dimens (again, thank you IE)
        var windowWidthNew = $(window).width(),
        windowHeightNew = $(window).height();
        // make sure that it is a true window resize
        // *we must check this because our dinosaur friend IE fires a window resize event when certain DOM elements
        // are resized. Can you just die already?*
        if (windowWidth !== windowWidthNew || windowHeight !== windowHeightNew) {
          // set the new window dimens
          windowWidth = windowWidthNew;
          windowHeight = windowHeightNew;
          // update all dynamic elements
          el.redrawSliderad();
          // Call user resize handler
          sliderad.settings.onSlideradResize.call(el, sliderad.active.index);
        }
      }
    };

    /**
     * Adds an aria-hidden=true attribute to each element
     *
     * @param startVisibleIndex (int)
     *  - the first visible element's index
     */
    var applyAriaHiddenAttributes = function(startVisibleIndex) {
      var numberOfSlidesShowing = getNumberSlidesShowing();
      // only apply attributes if the setting is enabled and not in ticker mode
      if (sliderad.settings.ariaHidden && !sliderad.settings.ticker) {
        // add aria-hidden=true to all elements
        sliderad.children.attr('aria-hidden', 'true');
        // get the visible elements and change to aria-hidden=false
        sliderad.children.slice(startVisibleIndex, startVisibleIndex + numberOfSlidesShowing).attr('aria-hidden', 'false');
      }
    };

    /**
     * Returns index according to present page range
     *
     * @param slideOndex (int)
     *  - the desired slide index
     */
    var setSlideIndex = function(slideIndex) {
      if (slideIndex < 0) {
        if (sliderad.settings.infiniteLoop) {
          return getPagerQty() - 1;
        }else {
          //we don't go to undefined slides
          return sliderad.active.index;
        }
      // if slideIndex is greater than children length, set active index to 0 (this happens during infinite loop)
      } else if (slideIndex >= getPagerQty()) {
        if (sliderad.settings.infiniteLoop) {
          return 0;
        } else {
          //we don't move to undefined pages
          return sliderad.active.index;
        }
      // set active index to requested slide
      } else {
        return slideIndex;
      }
    };

    /**
     * ===================================================================================
     * = PUBLIC FUNCTIONS
     * ===================================================================================
     */

    /**
     * Performs slide transition to the specified slide
     *
     * @param slideIndex (int)
     *  - the destination slide's index (zero-based)
     *
     * @param direction (string)
     *  - INTERNAL USE ONLY - the direction of travel ("prev" / "next")
     */
    el.goToSlide = function(slideIndex, direction) {
      // onSlideBefore, onSlideNext, onSlidePrev callbacks
      // Allow transition canceling based on returned value
      var performTransition = true,
      moveBy = 0,
      position = {left: 0, top: 0},
      lastChild = null,
      lastShowingIndex, eq, value, requestEl;
      // store the old index
      sliderad.oldIndex = sliderad.active.index;
      //set new index
      sliderad.active.index = setSlideIndex(slideIndex);

      // if plugin is currently in motion, ignore request
      if (sliderad.working || sliderad.active.index === sliderad.oldIndex) { return; }
      // declare that plugin is in motion
      sliderad.working = true;

      performTransition = sliderad.settings.onSlideBefore.call(el, sliderad.children.eq(sliderad.active.index), sliderad.oldIndex, sliderad.active.index);

      // If transitions canceled, reset and return
      if (typeof (performTransition) !== 'undefined' && !performTransition) {
        sliderad.active.index = sliderad.oldIndex; // restore old index
        sliderad.working = false; // is not in motion
        return;
      }

      if (direction === 'next') {
        // Prevent canceling in future functions or lack there-of from negating previous commands to cancel
        if (!sliderad.settings.onSlideNext.call(el, sliderad.children.eq(sliderad.active.index), sliderad.oldIndex, sliderad.active.index)) {
          performTransition = false;
        }
      } else if (direction === 'prev') {
        // Prevent canceling in future functions or lack there-of from negating previous commands to cancel
        if (!sliderad.settings.onSlidePrev.call(el, sliderad.children.eq(sliderad.active.index), sliderad.oldIndex, sliderad.active.index)) {
          performTransition = false;
        }
      }

      // check if last slide
      sliderad.active.last = sliderad.active.index >= getPagerQty() - 1;
      // update the pager with active class
      if (sliderad.settings.pager || sliderad.settings.pagerCustom) { updatePagerActive(sliderad.active.index); }
      // // check for direction control update
      if (sliderad.settings.controls) { updateDirectionControls(); }
      // if sliderad is set to mode: "fade"
      if (sliderad.settings.mode === 'fade') {
        // if adaptiveHeight is true and next height is different from current height, animate to the new height
        if (sliderad.settings.adaptiveHeight && sliderad.viewport.height() !== getViewportHeight()) {
          sliderad.viewport.animate({height: getViewportHeight()}, sliderad.settings.adaptiveHeightSpeed);
        }
        // fade out the visible child and reset its z-index value
        sliderad.children.filter(':visible').fadeOut(sliderad.settings.speed).css({zIndex: 0});
        // fade in the newly requested slide
        sliderad.children.eq(sliderad.active.index).css('zIndex', sliderad.settings.slideZIndex + 1).fadeIn(sliderad.settings.speed, function() {
          $(this).css('zIndex', sliderad.settings.slideZIndex);
          updateAfterSlideTransition();
        });
      // sliderad mode is not "fade"
      } else {
        // if adaptiveHeight is true and next height is different from current height, animate to the new height
        if (sliderad.settings.adaptiveHeight && sliderad.viewport.height() !== getViewportHeight()) {
          sliderad.viewport.animate({height: getViewportHeight()}, sliderad.settings.adaptiveHeightSpeed);
        }
        // if carousel and not infinite loop
        if (!sliderad.settings.infiniteLoop && sliderad.carousel && sliderad.active.last) {
          if (sliderad.settings.mode === 'horizontal') {
            // get the last child position
            lastChild = sliderad.children.eq(sliderad.children.length - 1);
            position = lastChild.position();
            // calculate the position of the last slide
            moveBy = sliderad.viewport.width() - lastChild.outerWidth();
          } else {
            // get last showing index position
            lastShowingIndex = sliderad.children.length - sliderad.settings.minSlides;
            position = sliderad.children.eq(lastShowingIndex).position();
          }
          // horizontal carousel, going previous while on first slide (infiniteLoop mode)
        } else if (sliderad.carousel && sliderad.active.last && direction === 'prev') {
          // get the last child position
          eq = sliderad.settings.moveSlides === 1 ? sliderad.settings.maxSlides - getMoveBy() : ((getPagerQty() - 1) * getMoveBy()) - (sliderad.children.length - sliderad.settings.maxSlides);
          lastChild = el.children('.bx-clone').eq(eq);
          position = lastChild.position();
        // if infinite loop and "Next" is clicked on the last slide
        } else if (direction === 'next' && sliderad.active.index === 0) {
          // get the last clone position
          position = el.find('> .bx-clone').eq(sliderad.settings.maxSlides).position();
          sliderad.active.last = false;
        // normal non-zero requests
        } else if (slideIndex >= 0) {
          //parseInt is applied to allow floats for slides/page
          requestEl = slideIndex * parseInt(getMoveBy());
          position = sliderad.children.eq(requestEl).position();
        }

        /* If the position doesn't exist
         * (e.g. if you destroy the sliderad on a next click),
         * it doesn't throw an error.
         */
        if (typeof (position) !== 'undefined') {
          value = sliderad.settings.mode === 'horizontal' ? -(position.left - moveBy) : -position.top;
          // plugin values to be animated
          setPositionProperty(value, 'slide', sliderad.settings.speed);
        } else {
          sliderad.working = false;
        }
      }
      if (sliderad.settings.ariaHidden) { applyAriaHiddenAttributes(sliderad.active.index * getMoveBy()); }
    };

    /**
     * Transitions to the next slide in the show
     */
    el.goToNextSlide = function() {
      // if infiniteLoop is false and last page is showing, disregard call
      if (!sliderad.settings.infiniteLoop && sliderad.active.last) { return; }
      var pagerIndex = parseInt(sliderad.active.index) + 1;
      el.goToSlide(pagerIndex, 'next');
    };

    /**
     * Transitions to the prev slide in the show
     */
    el.goToPrevSlide = function() {
      // if infiniteLoop is false and last page is showing, disregard call
      if (!sliderad.settings.infiniteLoop && sliderad.active.index === 0) { return; }
      var pagerIndex = parseInt(sliderad.active.index) - 1;
      el.goToSlide(pagerIndex, 'prev');
    };

    /**
     * Starts the auto show
     *
     * @param preventControlUpdate (boolean)
     *  - if true, auto controls state will not be updated
     */
    el.startAuto = function(preventControlUpdate) {
      // if an interval already exists, disregard call
      if (sliderad.interval) { return; }
      // create an interval
      sliderad.interval = setInterval(function() {
        if (sliderad.settings.autoDirection === 'next') {
          el.goToNextSlide();
        } else {
          el.goToPrevSlide();
        }
      }, sliderad.settings.pause);
      // if auto controls are displayed and preventControlUpdate is not true
      if (sliderad.settings.autoControls && preventControlUpdate !== true) { updateAutoControls('stop'); }
    };

    /**
     * Stops the auto show
     *
     * @param preventControlUpdate (boolean)
     *  - if true, auto controls state will not be updated
     */
    el.stopAuto = function(preventControlUpdate) {
      // if no interval exists, disregard call
      if (!sliderad.interval) { return; }
      // clear the interval
      clearInterval(sliderad.interval);
      sliderad.interval = null;
      // if auto controls are displayed and preventControlUpdate is not true
      if (sliderad.settings.autoControls && preventControlUpdate !== true) { updateAutoControls('start'); }
    };

    /**
     * Returns current slide index (zero-based)
     */
    el.getCurrentSlide = function() {
      return sliderad.active.index;
    };

    /**
     * Returns current slide element
     */
    el.getCurrentSlideElement = function() {
      return sliderad.children.eq(sliderad.active.index);
    };

    /**
     * Returns a slide element
     * @param index (int)
     *  - The index (zero-based) of the element you want returned.
     */
    el.getSlideElement = function(index) {
      return sliderad.children.eq(index);
    };

    /**
     * Returns number of slides in show
     */
    el.getSlideCount = function() {
      return sliderad.children.length;
    };

    /**
     * Return sliderad.working variable
     */
    el.isWorking = function() {
      return sliderad.working;
    };

    /**
     * Update all dynamic sliderad elements
     */
    el.redrawSliderad = function() {
      // resize all children in ratio to new screen size
      sliderad.children.add(el.find('.bx-clone')).outerWidth(getSlideWidth());
      // adjust the height
      sliderad.viewport.css('height', getViewportHeight());
      // update the slide position
      if (!sliderad.settings.ticker) { setSlidePosition(); }
      // if active.last was true before the screen resize, we want
      // to keep it last no matter what screen size we end on
      if (sliderad.active.last) { sliderad.active.index = getPagerQty() - 1; }
      // if the active index (page) no longer exists due to the resize, simply set the index as last
      if (sliderad.active.index >= getPagerQty()) { sliderad.active.last = true; }
      // if a pager is being displayed and a custom pager is not being used, update it
      if (sliderad.settings.pager && !sliderad.settings.pagerCustom) {
        populatePager();
        updatePagerActive(sliderad.active.index);
      }
      if (sliderad.settings.ariaHidden) { applyAriaHiddenAttributes(sliderad.active.index * getMoveBy()); }
    };

    /**
     * Destroy the current instance of the sliderad (revert everything back to original state)
     */
    el.destroySliderad = function() {
      // don't do anything if sliderad has already been destroyed
      if (!sliderad.initialized) { return; }
      sliderad.initialized = false;
      $('.bx-clone', this).remove();
      sliderad.children.each(function() {
        if ($(this).data('origStyle') !== undefined) {
          $(this).attr('style', $(this).data('origStyle'));
        } else {
          $(this).removeAttr('style');
        }
      });
      if ($(this).data('origStyle') !== undefined) {
        this.attr('style', $(this).data('origStyle'));
      } else {
        $(this).removeAttr('style');
      }
      $(this).unwrap().unwrap();
      if (sliderad.controls.el) { sliderad.controls.el.remove(); }
      if (sliderad.controls.next) { sliderad.controls.next.remove(); }
      if (sliderad.controls.prev) { sliderad.controls.prev.remove(); }
      if (sliderad.pagerEl && sliderad.settings.controls && !sliderad.settings.pagerCustom) { sliderad.pagerEl.remove(); }
      $('.bx-caption', this).remove();
      if (sliderad.controls.autoEl) { sliderad.controls.autoEl.remove(); }
      clearInterval(sliderad.interval);
      if (sliderad.settings.responsive) { $(window).unbind('resize', resizeWindow); }
      if (sliderad.settings.keyboardEnabled) { $(document).unbind('keydown', keyPress); }
      //remove self reference in data
      $(this).removeData('bxSlider');
    };

    /**
     * Reload the sliderad (revert all DOM changes, and re-initialize)
     */
    el.reloadSliderad = function(settings) {
      if (settings !== undefined) { options = settings; }
      el.destroySliderad();
      init();
      //store reference to self in order to access public functions later
      $(el).data('bxSlider', this);
    };

    init();

    $(el).data('bxSlider', this);

    // returns the current jQuery object
    return this;
  };

})(jQuery);
});