<?php
/**
 * Copyright © 2015 Sigosoft. All rights reserved.
 */

namespace Sigosoft\Sliderad\Controller\Adminhtml\Slides;

class NewAction extends \Sigosoft\Sliderad\Controller\Adminhtml\Slides
{

    public function execute()
    {
        $this->_forward('edit');
    }
}
