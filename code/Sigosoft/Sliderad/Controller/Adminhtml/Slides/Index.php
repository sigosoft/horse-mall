<?php
/**
 * Copyright © 2015 Sigosoft. All rights reserved.
 */

namespace Sigosoft\Sliderad\Controller\Adminhtml\Slides;

class Index extends \Sigosoft\Sliderad\Controller\Adminhtml\Slides
{
    /**
     * Items list.
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Sigosoft_Sliderad::sliderad');
        $resultPage->getConfig()->getTitle()->prepend(__('Manage Slides'));
        $resultPage->addBreadcrumb(__('Deverra'), __('Deverra'));
        $resultPage->addBreadcrumb(__('Slides'), __('Slides'));
        return $resultPage;
    }
}
