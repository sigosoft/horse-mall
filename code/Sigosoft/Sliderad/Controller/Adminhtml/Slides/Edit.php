<?php
/**
 * Copyright © 2015 Sigosoft. All rights reserved.
 */

namespace Sigosoft\Sliderad\Controller\Adminhtml\Slides;

class Edit extends \Sigosoft\Sliderad\Controller\Adminhtml\Slides
{

    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        $model = $this->_objectManager->create('Sigosoft\Sliderad\Model\Slides');

        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addError(__('This item no longer exists.'));
                $this->_redirect('sigosoft_sliderad/*');
                return;
            }
        }
        // set entered data if was error when we do save
        $data = $this->_objectManager->get('Magento\Backend\Model\Session')->getPageData(true);
        if (!empty($data)) {
            $model->addData($data);
        }
        $this->_coreRegistry->register('current_sigosoft_sliderad_slides', $model);
        $this->_initAction();
        $this->_view->getLayout()->getBlock('slides_slides_edit');
        $this->_view->renderLayout();
    }
}
