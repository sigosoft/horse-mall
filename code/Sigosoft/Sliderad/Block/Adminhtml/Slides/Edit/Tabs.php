<?php
/**
 * Copyright © 2015 Sigosoft. All rights reserved.
 */
namespace Sigosoft\Sliderad\Block\Adminhtml\Slides\Edit;

class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    /**
     * Constructor
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('sigosoft_sliderad_slides_edit_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Slide'));
    }
}
