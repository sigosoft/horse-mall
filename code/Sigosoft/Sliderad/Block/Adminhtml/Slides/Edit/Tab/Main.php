<?php
/**
 * Copyright © 2015 Sigosoft. All rights reserved.
 */

// @codingStandardsIgnoreFile

namespace Sigosoft\Sliderad\Block\Adminhtml\Slides\Edit\Tab;

use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Widget\Tab\TabInterface;
use Magento\Framework\View\Layout;
use Magento\Framework\ObjectManagerInterface;

class Main extends Generic implements TabInterface
{
     /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $_resource;
    protected $_systemStore;
    protected $connection;
    protected $_blockFactory;
    protected $_objectManager;
    
    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Store\Model\System\Store $systemStore
     * @param array $data
     */

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Store\Model\System\Store $systemStore,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formfactory,
        ObjectManagerInterface $objectManagerInterface,
        Layout $layout,  
        \Magento\Framework\App\ResourceConnection $resource
    ) {
        $this->_resource = $resource;
        $this->_systemStore = $systemStore;
        $this->_objectManager = $objectManagerInterface;
        $this->_blockFactory = $layout;
        parent::__construct($context, $registry, $formfactory);
    }
    /**
     * {@inheritdoc}
     */
    public function getTabLabel()
    {
        return __('Slide Information');
    }

    /**
     * {@inheritdoc}
     */
    public function getTabTitle()
    {
        return __('Slide Information');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }
    protected function _isAllowedAction($resourceId) {
        return $this->_authorization->isAllowed($resourceId);
    }

    /**
     * Prepare form before rendering HTML
     *
     * @return $this
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareForm()
    {
        $model = $this->_coreRegistry->registry('current_sigosoft_sliderad_slides');
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('slides_');
        if ($this->_isAllowedAction('Sigosoft_Sliderad::slides')) {
            $isElementDisabled = false;
        } else {
            $isElementDisabled = true;
        }
        $form = $this->_formFactory->create();
        $objectManager = $this->_objectManager;
        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Slide Information')]);
        $fieldset->addType('image', '\Sigosoft\Sliderad\Block\Adminhtml\Slides\Helper\Image');
        $fieldset->addType('category', '\Sigosoft\Sliderad\Block\Adminhtml\Slides\Helper\Category');
        if ($model->getId()) {
            $fieldset->addField('id', 'hidden', ['name' => 'id']);
        }
        $fieldset->addField('title', 'text', [
            'name' => 'title', 
            'label' => __('Slide Name'), 
            'title' => __('Slide Name'), 
            'required' => true
            ]
        );
        $fieldset->addField('slide', 'image',[
            'name' => 'slide', 
            'label' => __('Slide Image'), 
            'title' => __('Slide Image'), 
            'required' => true
            ]
        );
        $fieldset->addField(
            'mobile',
            'image',
            ['name' => 'mobile', 'label' => __('Mobile'), 'title' => __('Mobile'), 'required' => false]
        );
        $fieldset->addField('status', 'select',[
            'name' => 'status', 
            'label' => __('Status'), 
            'title' => __('Status'), 
            'values' => array(
                0 => array('label' => 'Enable', 'value' => '1'), 
                1 => array('label' => 'Disable', 'value' => '0')
                ), 
            'required' => true
            ]
        );

        $linkType = $fieldset->addField(
            'link_type', 'select', ['name' => 'link_type',
        'label' => __('Link Type'),
        'title' => __('Link Type'),
        'values' => $objectManager->get('Sigosoft\Sliderad\Model\System\Config\LinkType')->toOptionArray(),
        'onchange' => 'CheckType(this)',
        'required' => false]
        );

        $brand = $fieldset->addField(
                'brand', 'select', [
            'label' => __('Brand'),
            'title' => __('Brand'),
            'name' => 'brand',
            'required' => false,
            'values' => $this->getBrands(),
            'disabled' => $isElementDisabled
                ]
        );
        
        $external = $fieldset->addField(
                'external_link', 'text', [
            'label' => __('External Link'),
            'title' => __('External Link'),
            'name' => 'external_link',
            'required' => false
                ]
        );
         

        $data_type = $fieldset->addField(
            'data_type', 'text', [
        'label' => __('Data Type'),
        'title' => __('Data Type'),
        'class' => 'data_type',
        'name' => 'data_type',
        'note' => "With Menu Type: Enternal Link <br /> Http link can is full link \"http://magento.com, ...\" or short link \"magento.com, ...\". <br/>" .
        "Note: With link type \"https://www.google.com.vn\", \"https://...\", you only can use short link \"google.com.vn\", ..."
            ]
        );

        $addWidget = $this->_blockFactory->createBlock('\Sigosoft\Sliderad\Block\Adminhtml\Widget\AddField');
        $addWidget->addFieldWidget(
                [
            'data' => [
                '@' => ['type' => 'complex'],
                'id' => 'product_id',
                'sort_order' => '10',
                'label' => 'Product',
                'required' => false,
                'helper_block' => [
                    'data' => [
                        'button' => [
                            'open' => __('Select Product...')
                        ]
                    ],
                    'type' => 'Magento\Catalog\Block\Adminhtml\Product\Widget\Chooser'
                ]
            ]
                ], $fieldset
        );
        $addWidget->addFieldWidget(
                [
            'data' => [
                '@' => ['type' => 'complex'],
                'id' => 'category_id',
                'sort_order' => '11',
                'label' => 'Category',
                'required' => false,
                'helper_block' => [
                    'data' => [
                        'button' => [
                            'open' => __('Select Category...')
                        ]
                    ],
                    'type' => 'Magento\Catalog\Block\Adminhtml\Category\Widget\Chooser'
                ]
            ]
                ], $fieldset
        );
        $js_type = "";
        if ($model->getId()) {
            $type_val = $model[$linkType->getId()];
            $data_type_val = $model[$data_type->getId()];
            $js_type = '
					data_val[' . $type_val . '] = "' . $data_type_val . '";
					CheckType($(\'' . $linkType->getId() . '\'));
			';
        }
        $linkType->setAfterElementHtml('
					<script type="text/javascript">
						// check type
						var data_val = new Array();
						require([
							"jquery",
							"tinymce",
							"Magento_Ui/js/modal/modal",
							"prototype",
							"mage/adminhtml/events"
						], function(jQuery, tinyMCE, modal){
							$$("div[id^=\'' . 'box_\']").each( function(element){
								element.up().up().up().hide();
							});
							/*$$("[id^=\'' . 'content\']").each(function(element){
								element.up().up().up().hide();
							});*/
							$$(".megamenu_content").each(function(element){
								element.up().up().up().hide();
							});
							$$(".data_type").each(function(element){
								element.up().up().hide();
								element.removeClassName("required-entry");
							});
							$(\'' . $linkType->getId() . '\').observe("focus",function(event){
								var element = Event.element(event);
								data_val[element.value] = $$(".data_type")[0].value;
							});
                                                        ' . $js_type . '
						});
						function CheckType(element){
							type = element.value;
							if(typeof(data_val[type]) !="undefined"){
								$$(".data_type")[0].value = data_val[type];
							}
							else{
								$$(".data_type")[0].value ="";
							}
							$$("div[id^=\'' . 'box_\']").each(function(element){
								element.up().up().up().hide();
					   		});
							$$(".data_type").each(function(element){
								element.up().up().hide();
								element.removeClassName("required-entry");
							});
							/*$$("[id^=\'' . 'content\']").each(function(element){
								element.up().up().up().hide();
								element.removeClassName("required-entry");
							});*/
							$$(".megamenu_content").each(function(element){
								element.up().up().up().hide();
								element.removeClassName("required-entry");
							});
							if(type==3){
								$$("div[id^=\'' . 'box_product_id\']")[0].up().up().up().show();
							}
                                                        if(type==4){
								$$("div[id^=\'' . 'box_category_id\']")[0].up().up().up().show();
							}
                                                        
						}
					</script>
		');
        /**
         * Check is single store mode
         */
        if (!$this->_storeManager->isSingleStoreMode()) {
            $field = $fieldset->addField(
                    'store_id', 'multiselect', [
                'name' => 'stores[]',
                'label' => __('Store View'),
                'title' => __('Store View'),
                'required' => true,
                'values' => $this->_systemStore->getStoreValuesForForm(false, true),
                'disabled' => $isElementDisabled
                    ]
            );
            $renderer = $this->getLayout()->createBlock(
                    'Magento\Backend\Block\Store\Switcher\Form\Renderer\Fieldset\Element'
            );
            $field->setRenderer($renderer);
        } else {
            $fieldset->addField(
                    'store_id', 'hidden', ['name' => 'stores[]', 'value' => $this->_storeManager->getStore(true)->getId()]
            );
            $model->setStoreId($this->_storeManager->getStore(true)->getId());
        }



        $this->setChild(
                'form_after', $this->getLayout()->createBlock('Magento\Backend\Block\Widget\Form\Element\Dependence')
                        ->addFieldMap($linkType->getHtmlId(), $linkType->getName())
                        ->addFieldMap($brand->getHtmlId(), $brand->getName())
                        ->addFieldDependence($brand->getName(), $linkType->getName(), '5')
                        ->addFieldMap($external->getHtmlId(), $external->getName())
                        ->addFieldDependence($external->getName(), $linkType->getName(), '2')
        );

        $this->_eventManager->dispatch('adminhtml_sliderad_edit_tab_main_prepare_form', ['form' => $form]);

        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }

    protected function getConnection() {
        if (!$this->connection) {
            $this->connection = $this->_resource->getConnection('core_write');
        }
        return $this->connection;
    }

    public function getBrandsList() {
        $storeId = $this->getCurrentStoreName();
        $eav_attribute_option = $this->_resource->getTableName('eav_attribute_option');
        $eav_attribute_option_value = $this->_resource->getTableName('eav_attribute_option_value');
        $brands = $this->getConnection()->fetchAll('SELECT ' . $eav_attribute_option . '.option_id as id,' . $eav_attribute_option_value . '.value as name  FROM ' . $eav_attribute_option_value . ' JOIN ' . $eav_attribute_option . ' ON ' . $eav_attribute_option_value . '.option_id = ' . $eav_attribute_option . '.option_id WHERE ' . $eav_attribute_option . '.attribute_id = 83 AND ' . $eav_attribute_option_value . '.store_id =' . $storeId);
        return $brands;
    }

    public function getCurrentStoreName() {
        return $this->_storeManager->getStore()->getId();
    }

    public function getBrands() {
        $options = $this->getBrandsList();
        if (sizeof($options) > 0) {
            $optionsArray = array(array('value' => 0, 'label' => 'Please Select'));
            foreach ($options as $optionList) {

                $optionsArray[] = array('value' => $optionList['id'], 'label' => $optionList['name']);
            }
        } else {
            $optionsArray = array(0 => array('label' => 'NIL', 'value' => '0'));
        }
        return $optionsArray;
    }


}
