<?php
/**
 * Copyright © 2015 Sigosoft. All rights reserved.
 */

namespace Sigosoft\Sliderad\Api;

interface SlideradInterface
{
	/**
	*
	* @api
	* @return $this
	*/
    public function slides();
}