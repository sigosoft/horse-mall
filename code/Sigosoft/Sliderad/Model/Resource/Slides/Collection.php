<?php
/**
 * Copyright © 2015 Sigosoft. All rights reserved.
 */

namespace Sigosoft\Sliderad\Model\Resource\Slides;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Sigosoft\Sliderad\Model\Slides', 'Sigosoft\Sliderad\Model\Resource\Slides');
    }
}
