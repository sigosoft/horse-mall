<?php
/**
 * Copyright © 2015 Sigosoft. All rights reserved.
 */

namespace Sigosoft\Sliderad\Model;

use Sigosoft\Sliderad\Api\SlideradInterface;

class SlideradApi implements SlideradInterface
{
	protected $_storeManager;
	protected $_slidesFactory;

	public function __construct(
		\Magento\Store\Model\StoreManagerInterface $storeManager,
		\Sigosoft\Sliderad\Model\SlidesFactory $slidesFactory
	) {
		$this->_storeManager = $storeManager;
		$this->_slidesFactory = $slidesFactory;
	}
	public function getMediaUrl()
	{
		return $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
	}
	/**
	*
	* @api
	* @return $this
	*/
	public function slides() {
		$slides = array();
		$slidesCollection = $this->_slidesFactory->create()->getCollection();
		if(count($slidesCollection)){
			foreach ($slidesCollection as $slide) {
				$data = array(
					"id"=>$slide->getId(),
					"title"=>$slide->getTitle(),
					"mobile"=>$this->getMediaUrl().'sigosoft/sliderad/slides/image'.$slide->getMobile(),
					"link_type" => $this->getLinkType($slide),
                    "data" => $this->getLink($slide),
				);
				$slides[] = $data;
			}
			return $slides;
		}
		else{
			return $slides;
		}
	}
	public function getLinkType($slide) {
        $linkType = "";
        switch($slide->getLinkType()){
            case \Sigosoft\Sliderad\Model\System\Config\LinkType::NORMAL :
                $linkType = "normal";
                break;
            case \Sigosoft\Sliderad\Model\System\Config\LinkType::EXTERNALLINK :
                $linkType = "external_link";
                break;
            case \Sigosoft\Sliderad\Model\System\Config\LinkType::PRODUCT :
                $linkType = "product";
                break;
            case \Sigosoft\Sliderad\Model\System\Config\LinkType::CATEGORY :
                $linkType = "category";
                break;
            case \Sigosoft\Sliderad\Model\System\Config\LinkType::BRAND :
                $linkType = "brand";
                break;
        }
        return $linkType;
    }
    
    public function getLink($slide) {
        $link = "";
        switch($slide->getLinkType()){
            case \Sigosoft\Sliderad\Model\System\Config\LinkType::NORMAL :
                $link = "";
                break;
            case \Sigosoft\Sliderad\Model\System\Config\LinkType::EXTERNALLINK :
                $link = $slide->getExternalLink();
                break;
            case \Sigosoft\Sliderad\Model\System\Config\LinkType::PRODUCT :
            case \Sigosoft\Sliderad\Model\System\Config\LinkType::CATEGORY :
                $datatype = $slide->getDataType();
                $link = basename($datatype);
                break;
            case \Sigosoft\Sliderad\Model\System\Config\LinkType::BRAND :
                $link = $slide->getBrand();
                break;
        }
        return $link;
    }

}