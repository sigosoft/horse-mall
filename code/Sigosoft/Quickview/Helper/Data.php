<?php
namespace Sigosoft\Quickview\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Catalog\Model\ResourceModel\Eav\Attribute $attributes,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    )
    {
        parent::__construct($context);
        $this->attributes = $attributes;
        $this->scopeConfig = $scopeConfig;
    }

    public function getAttributes() {
        $attributeIds = $this->scopeConfig->getValue('quickview/product/attribute', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return $this->attributes->getCollection()
                        ->addFieldToFilter('attribute_id', array('in',explode(',', $attributeIds)));
    }

}