<?php
/**
 * Copyright © 2015 Sigosoft. All rights reserved.
 */

namespace Sigosoft\MainMenu\Controller\Adminhtml\Navigator;

class NewAction extends \Sigosoft\MainMenu\Controller\Adminhtml\Navigator
{

    public function execute()
    {
        $this->_forward('edit');
    }
}
