<?php
/**
 * Copyright © 2015 Sigosoft. All rights reserved.
 */

namespace Sigosoft\MainMenu\Controller\Adminhtml\Navigator;

class Index extends \Sigosoft\MainMenu\Controller\Adminhtml\Navigator
{
    /**
     * Items list.
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Sigosoft_MainMenu::navigator');
        $resultPage->getConfig()->getTitle()->prepend(__('Sigosoft Main Menu'));
        $resultPage->addBreadcrumb(__('Main Menu'), __('Main Menu '));
        $resultPage->addBreadcrumb(__('Main Menu'), __('Main Menu'));
        return $resultPage;
    }
}
