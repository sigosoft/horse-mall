<?php
/**
 * Copyright © 2015 Sigosoft. All rights reserved.
 */

namespace Sigosoft\MainMenu\Controller\Adminhtml\Navigator;

class Edit extends \Sigosoft\MainMenu\Controller\Adminhtml\Navigator
{

    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        $model = $this->_objectManager->create('Sigosoft\MainMenu\Model\Navigator');

        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addError(__('This item no longer exists.'));
                $this->_redirect('sigosoft_mainmenu/*');
                return;
            }
        }
        // set entered data if was error when we do save
        $data = $this->_objectManager->get('Magento\Backend\Model\Session')->getPageData(true);
        if (!empty($data)) {
            $model->addData($data);
        }
        $this->_coreRegistry->register('current_sigosoft_mainmenu_navigator', $model);
        $this->_initAction();
        $this->_view->getLayout()->getBlock('navigator_navigator_edit');
        $this->_view->renderLayout();
    }
}
