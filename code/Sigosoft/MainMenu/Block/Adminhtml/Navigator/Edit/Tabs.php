<?php
/**
 * Copyright © 2015 Sigosoft. All rights reserved.
 */
namespace Sigosoft\MainMenu\Block\Adminhtml\Navigator\Edit;

class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    /**
     * Constructor
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('sigosoft_mainmenu_navigator_edit_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Menu'));
    }
}
