<?php
/**
 * Copyright © 2015 Sigosoft. All rights reserved.
 */
namespace Sigosoft\MainMenu\Block\Adminhtml;

class Navigator extends \Magento\Backend\Block\Widget\Grid\Container
{
    /**
     * Constructor
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_controller = 'navigator';
        $this->_headerText = __('Menu');
        $this->_addButtonLabel = __('Add New Menu');
        parent::_construct();
    }
}
