<?php

namespace Sigosoft\MainMenu\Block;

use Magento\Framework\View\Element\Template;

class Navigator extends Template {

    protected $_brandsFactory;

    public function __construct(
    \Magento\Framework\View\Element\Template\Context $context,
    \Zend_Filter_Interface $templateProcessor,
    //\Magento\Store\Model\StoreManagerInterface $storeManager,
            \Sigosoft\MainMenu\Model\NavigatorFactory $navigatorFactory
    ) {
        //$this->_storeManager = $storeManager;
        $this->_storeManager = $context->getStoreManager();
        $this->_navigatorFactory = $navigatorFactory;
        $this->templateProcessor = $templateProcessor;
        parent::__construct($context);
    }

    public function getMainMenu() {
        $navigatorCollection = $this->_navigatorFactory->create()->getCollection()->addFieldToFilter('status', 1)->setOrder('sort_order', 'ASC');
        return $navigatorCollection;
    }
    public function filterOutputHtml($string) 
    {
        return $this->templateProcessor->filter($string);
    }

    public function getMediaUrl() {
        return $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
    }

}
