<?php
/**
 * Copyright © 2015 Sigosoft. All rights reserved.
 */

namespace Sigosoft\MainMenu\Model\Resource\Navigator;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Sigosoft\MainMenu\Model\Navigator', 'Sigosoft\MainMenu\Model\Resource\Navigator');
    }
}
