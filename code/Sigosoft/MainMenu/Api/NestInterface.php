<?php
/**
 * Copyright © 2015 Sigosoft. All rights reserved.
 */

namespace Sigosoft\MainMenu\Api;

interface NestInterface
{
	/**
	*
	* @api
	* @return $this
	*/
    public function nest();
}