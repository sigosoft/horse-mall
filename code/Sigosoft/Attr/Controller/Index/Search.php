<?php
namespace Sigosoft\Attr\Controller\Index;

class Search extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
    public function execute()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $baseurl = $storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB);
        $postedData = $this->getRequest()->getPostValue();
        $city = $_POST['City'];
        $breed = $_POST['Breed'];
        $disciplin = $_POST['Disciplins'];
        $keyword = $_POST['Keyword'];
        if($breed!='Breed'){
            $br = "breeds=$breed";
        }
        else{
            $br= "";
        }
        if($disciplin!='Disciplines'){
            $dis = "disciplins=$disciplin";
        }
        else{
            $dis= "";
        }
        if($city!='City'){
            $cit = "city=$city";
        }
        else{
            $cit= "";
        }
        $url = "$baseurl/horses.html?$br&$dis&$cit&q=$keyword";
        return $this->resultRedirectFactory->create()
                ->setPath($url); 
    }
}