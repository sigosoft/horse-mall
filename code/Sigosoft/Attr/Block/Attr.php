<?php
namespace Sigosoft\Attr\Block;
 
class Attr extends \Magento\Framework\View\Element\Template
{
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Eav\Model\Config $eavConfig,
        array $data = []
    ) {
        $this->eavConfig = $eavConfig;
        parent::__construct($context, $data);
    }
 
    /**
     * Entity Type Catalog
     * Attribute code color
     * @return array
     */
    public function getBreedList()
    {
        $attribute = $this->eavConfig->getAttribute('catalog_product', 'breeds');
        return $attribute->getSource()->getAllOptions();
    }
    public function getCityList()
    {
        $attribute = $this->eavConfig->getAttribute('catalog_product', 'city');
        return $attribute->getSource()->getAllOptions();
    }
    public function getDisciplinsList()
    {
        $attribute = $this->eavConfig->getAttribute('catalog_product', 'disciplins');
        return $attribute->getSource()->getAllOptions();
    }
}