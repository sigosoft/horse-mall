<?php
/**
 * Copyright © 2015 Sigosoft. All rights reserved.
 */

namespace Sigosoft\Banner\Api;

interface BannerInterface
{
	/**
	*
	* @api
	* @return $this
	*/
    public function banners();
}