<?php

/**
 * Copyright © 2015 Sigosoft. All rights reserved.
 */

namespace Sigosoft\Banner\Model;

use Sigosoft\Banner\Api\BannerInterface;

class BannerApi implements BannerInterface {

    protected $_storeManager;
    protected $_brandsFactory;

    public function __construct(
    \Magento\Store\Model\StoreManagerInterface $storeManager, \Sigosoft\Banner\Model\ResourceModel\Banner\CollectionFactory $collectionFactory
    ) {
        $this->_storeManager = $storeManager;
        $this->collectionFactory = $collectionFactory;
    }

    public function getMediaUrl() {
        return $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
    }

    /**
     *
     * @api
     * @return $this
     */
    public function banners() {
        $banners = array();
        $collection = $this->collectionFactory->create()
                ->addFieldToFilter('status', 1)
                ->addFieldToFilter('banner_type', array("in" => ['4', '5']))
                ->setOrder('sort_order', 'ASC');
        if (count($collection)) {
            foreach ($collection as $banner) {
                $bannerType = '';
                if ($banner->getBannerType() == \Sigosoft\Banner\Model\System\Config\BannerType::AD_BANNER_LEFT) {
                    $bannerType = 'full';
                } else if ($banner->getBannerType() == \Sigosoft\Banner\Model\System\Config\BannerType::AD_BANNER_RIGHT) {
                    $bannerType = 'half';
                }
                $data = array(
                    "id" => $banner->getId(),
                    "name" => $banner->getName(),
                    "image" => $this->getMediaUrl() . $banner->getBannerImage(),
                    "banner_type" => $bannerType,
                    "link_type" => $this->getLinkType($banner),
                    "data" => $this->getLink($banner),
                );
                $banners[] = $data;
            }
            return $banners;
        } else {
            return $banners;
        }
    }

    public function getLinkType($banner) {
        $linkType = "";
        switch($banner->getLinkType()){
            case \Sigosoft\Banner\Model\Config\Source\LinkType::NORMAL :
                $linkType = "normal";
                break;
            case \Sigosoft\Banner\Model\Config\Source\LinkType::EXTERNALLINK :
                $linkType = "external_link";
                break;
            case \Sigosoft\Banner\Model\Config\Source\LinkType::PRODUCT :
                $linkType = "product";
                break;
            case \Sigosoft\Banner\Model\Config\Source\LinkType::CATEGORY :
                $linkType = "category";
                break;
            case \Sigosoft\Banner\Model\Config\Source\LinkType::BRAND :
                $linkType = "brand";
                break;
        }
        return $linkType;
    }
    
    public function getLink($banner) {
        $link = "";
        switch($banner->getLinkType()){
            case \Sigosoft\Banner\Model\Config\Source\LinkType::NORMAL :
                $link = "";
                break;
            case \Sigosoft\Banner\Model\Config\Source\LinkType::EXTERNALLINK :
                $link = $banner->getExternalLink();
                break;
            case \Sigosoft\Banner\Model\Config\Source\LinkType::PRODUCT :
            case \Sigosoft\Banner\Model\Config\Source\LinkType::CATEGORY :
                $datatype = $banner->getDataType();
                $link = basename($datatype);
                break;
            case \Sigosoft\Banner\Model\Config\Source\LinkType::BRAND :
                $link = $banner->getBrand();
                break;
        }
        return $link;
    }

}
