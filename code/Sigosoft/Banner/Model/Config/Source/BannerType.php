<?php

namespace Sigosoft\Banner\Model\Config\Source;

class BannerType implements \Magento\Framework\Option\ArrayInterface {

    const LEFT_BANNER = 1;
    const RIGHT_TOP_BANNER = 2;
    const RIGHT_BOTTOM_BANNER = 3;
    const MIDDLE_TOP_BANNER = 4;
    const MIDDLE_BOTTOM_BANNER = 5;
    const AD_BANNER_LEFT = 6;
    const AD_BANNER_RIGHT = 7;
    const AD_DETAIL_PAGE = 8;

    public function getOptionArray() {
        return [
            self::LEFT_BANNER => __('Left banner'),
            self::RIGHT_TOP_BANNER => __('Right top banner'),
            self::RIGHT_BOTTOM_BANNER => __('Right bottom banner'),
            self::MIDDLE_TOP_BANNER => __('Middle top banner'),
            self::MIDDLE_BOTTOM_BANNER => __('Middle bottom banner'),
            self::AD_BANNER_LEFT => __('Ad banner left'),
            self::AD_BANNER_RIGHT => __('Ad banner right'),
            self::AD_DETAIL_PAGE => __('Ad detail page')
        ];
    }

    public function toOptionArray() {
        return [
            [
                'value' => self::LEFT_BANNER,
                'label' => __('Left banner'),
            ],
            [
                'value' => self::RIGHT_TOP_BANNER,
                'label' => __('Right top banner'),
            ],
            [
                'value' => self::RIGHT_BOTTOM_BANNER,
                'label' => __('Right bottom banner'),
            ],
            [
                'value' => self::MIDDLE_TOP_BANNER,
                'label' => __('Middle top banner'),
            ],
            [
                'value' => self::MIDDLE_BOTTOM_BANNER,
                'label' => __('Middle bottom banner'),
            ],
            [
                'value' => self::AD_BANNER_LEFT,
                'label' => __('Ad banner left'),
            ],
            [
                'value' => self::AD_BANNER_RIGHT,
                'label' => __('Ad banner right'),
            ],
            [
                'value' => self::AD_DETAIL_PAGE,
                'label' => __('Ad detail page'),
            ]
        ];
    }

}