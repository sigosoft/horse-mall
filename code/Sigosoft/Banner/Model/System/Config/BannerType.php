<?php
 
namespace Sigosoft\Banner\Model\System\Config;
 
use Magento\Framework\Option\ArrayInterface;
 
class BannerType implements ArrayInterface
{
	const LEFT_BANNER = 1;
	const RIGHT_TOP_BANNER = 2;
        const RIGHT_BOTTOM_BANNER = 3;
        const MIDDLE_TOP_BANNER = 4;
	const MIDDLE_BOTTOM_BANNER = 5;
	const AD_BANNER_LEFT = 6;
	const AD_BANNER_RIGHT = 7;
	const AD_DETAIL_PAGE = 8;
	
	/**
	* @return array
	*/
	public function toOptionArray()
	{
		$options = [
			self::LEFT_BANNER => __('Left banner'),
			self::RIGHT_TOP_BANNER => __('Right top banner'),
			self::RIGHT_BOTTOM_BANNER => __('Right bottom banner'),
			self::MIDDLE_TOP_BANNER => __('Middle top banner'),
			self::MIDDLE_BOTTOM_BANNER => __('Middle bottom banner'),
			self::AD_BANNER_LEFT => __('Mobile full banner'),
			self::AD_BANNER_RIGHT => __('Mobile half banner'),
            self::AD_DETAIL_PAGE => __('Ad detail page')
		];
 
		return $options;
	}
}
