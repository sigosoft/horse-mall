<?php

/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Sigosoft\Banner\Model\Banner\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class Status
 */
class BannerType implements OptionSourceInterface {

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray() {

        $availableOptions = array('1' => 'Left banner', '2' => 'Right top banner','3'=> 'Right bottom banner','4'=> 'Middle top banner','5'=> 'Middle bottom banner','6'=>'Ad Banner Left','7'=>'Ad Banner Right','8'=>'Detail Page Banner');

        $options = [];
        foreach ($availableOptions as $key => $label) {
            $options[] = [
                'label' => $label,
                'value' => $key,
            ];
        }
        return $options;
    }

}
