<?php
/**
 * Copyright © 2015 Sigosoft. All rights reserved.
 */

namespace Sigosoft\Slider\Api;

interface SliderInterface
{
	/**
	*
	* @api
	* @return $this
	*/
    public function slides();
}