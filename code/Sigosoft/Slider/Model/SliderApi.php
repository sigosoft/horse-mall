<?php
/**
 * Copyright © 2015 Sigosoft. All rights reserved.
 */

namespace Sigosoft\Slider\Model;

use Sigosoft\Slider\Api\SliderInterface;

class SliderApi implements SliderInterface
{
	protected $_storeManager;
	protected $_slidesFactory;

	public function __construct(
		\Magento\Store\Model\StoreManagerInterface $storeManager,
		\Sigosoft\Slider\Model\SlidesFactory $slidesFactory
	) {
		$this->_storeManager = $storeManager;
		$this->_slidesFactory = $slidesFactory;
	}
	public function getMediaUrl()
	{
		return $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
	}
	/**
	*
	* @api
	* @return $this
	*/
	public function slides() {
		$slides = array();
		$slidesCollection = $this->_slidesFactory->create()->getCollection();
		if(count($slidesCollection)){
			foreach ($slidesCollection as $slide) {
				$data = array(
					"id"=>$slide->getId(),
					"title"=>$slide->getTitle(),
					"mobile"=>$this->getMediaUrl().'sigosoft/slider/slides/image'.$slide->getMobile(),
					"link_type" => $this->getLinkType($slide),
                    "data" => $this->getLink($slide),
				);
				$slides[] = $data;
			}
			return $slides;
		}
		else{
			return $slides;
		}
	}
	public function getLinkType($slide) {
        $linkType = "";
        switch($slide->getLinkType()){
            case \Sigosoft\Slider\Model\System\Config\LinkType::NORMAL :
                $linkType = "normal";
                break;
            case \Sigosoft\Slider\Model\System\Config\LinkType::EXTERNALLINK :
                $linkType = "external_link";
                break;
            case \Sigosoft\Slider\Model\System\Config\LinkType::PRODUCT :
                $linkType = "product";
                break;
            case \Sigosoft\Slider\Model\System\Config\LinkType::CATEGORY :
                $linkType = "category";
                break;
            case \Sigosoft\Slider\Model\System\Config\LinkType::BRAND :
                $linkType = "brand";
                break;
        }
        return $linkType;
    }
    
    public function getLink($slide) {
        $link = "";
        switch($slide->getLinkType()){
            case \Sigosoft\Slider\Model\System\Config\LinkType::NORMAL :
                $link = "";
                break;
            case \Sigosoft\Slider\Model\System\Config\LinkType::EXTERNALLINK :
                $link = $slide->getExternalLink();
                break;
            case \Sigosoft\Slider\Model\System\Config\LinkType::PRODUCT :
            case \Sigosoft\Slider\Model\System\Config\LinkType::CATEGORY :
                $datatype = $slide->getDataType();
                $link = basename($datatype);
                break;
            case \Sigosoft\Slider\Model\System\Config\LinkType::BRAND :
                $link = $slide->getBrand();
                break;
        }
        return $link;
    }

}