<?php
namespace Sigosoft\Slider\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements UpgradeSchemaInterface {

    /**
     * {@inheritdoc}
     */
    public function upgrade(
    SchemaSetupInterface $setup, ModuleContextInterface $context
    ) {
        $installer = $setup;

        $installer->startSetup();
        if (version_compare($context->getVersion(), '1.0.1', '<')) {
            $installer->getConnection()->addColumn(
                    $installer->getTable('sigosoft_slider_slides'), 'mobile', [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                ['nullable' => false, 'default' => ''],
                'comment' => 'mobile'
                    ]
            );
        }
        if (version_compare($context->getVersion(), '1.0.2', '<')) {
            $installer->getConnection()->addColumn(
                $installer->getTable('sigosoft_slider_slides'),'link_type',[
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    ['nullable' => false, 'default' => ''],
                    'comment' => 'Link Type'
                    ]
            );
        }
        if (version_compare($context->getVersion(), '1.0.3', '<')) {
            $installer->getConnection()->addColumn(
                $installer->getTable('sigosoft_slider_slides'),'data_type',[
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    ['nullable' => false, 'default' => ''],
                    'comment' => 'Data Type'
                    ]
            );
        }
        if (version_compare($context->getVersion(), '1.0.4', '<')) {
            $installer->getConnection()->addColumn(
                $installer->getTable('sigosoft_slider_slides'),'brand',[
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    ['nullable' => false, 'default' => ''],
                    'comment' => 'Brand Id'
                    ]
            );
        }
        if (version_compare($context->getVersion(), '1.0.5', '<')) {
            $installer->getConnection()->addColumn(
                $installer->getTable('sigosoft_slider_slides'),'external_link',[
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    ['nullable' => false, 'default' => ''],
                    'comment' => 'External Link'
                    ]
            );
        }
        if (version_compare($context->getVersion(), '1.0.7', '<')) {
            $setup->getConnection()->dropColumn($setup->getTable('sigosoft_slider_slides'), 'link');
            $installer->endSetup();
    
        }
        
    }

}
