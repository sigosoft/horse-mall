<?php
/**
 * Copyright © 2015 Sigosoft. All rights reserved.
 */

namespace Sigosoft\Slider\Controller\Adminhtml\Slides;

class Index extends \Sigosoft\Slider\Controller\Adminhtml\Slides
{
    /**
     * Items list.
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Sigosoft_Slider::slider');
        $resultPage->getConfig()->getTitle()->prepend(__('Manage Slides'));
        $resultPage->addBreadcrumb(__('Deverra'), __('Deverra'));
        $resultPage->addBreadcrumb(__('Slides'), __('Slides'));
        return $resultPage;
    }
}
