<?php
/**
 * Copyright © 2015 Sigosoft. All rights reserved.
 */

namespace Sigosoft\Slider\Controller\Adminhtml\Slides;

class NewAction extends \Sigosoft\Slider\Controller\Adminhtml\Slides
{

    public function execute()
    {
        $this->_forward('edit');
    }
}
