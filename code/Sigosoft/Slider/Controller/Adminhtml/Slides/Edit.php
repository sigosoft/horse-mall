<?php
/**
 * Copyright © 2015 Sigosoft. All rights reserved.
 */

namespace Sigosoft\Slider\Controller\Adminhtml\Slides;

class Edit extends \Sigosoft\Slider\Controller\Adminhtml\Slides
{

    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        $model = $this->_objectManager->create('Sigosoft\Slider\Model\Slides');

        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addError(__('This item no longer exists.'));
                $this->_redirect('sigosoft_slider/*');
                return;
            }
        }
        // set entered data if was error when we do save
        $data = $this->_objectManager->get('Magento\Backend\Model\Session')->getPageData(true);
        if (!empty($data)) {
            $model->addData($data);
        }
        $this->_coreRegistry->register('current_sigosoft_slider_slides', $model);
        $this->_initAction();
        $this->_view->getLayout()->getBlock('slides_slides_edit');
        $this->_view->renderLayout();
    }
}
