<?php
/**
 * Copyright © 2015 Sigosoft. All rights reserved.
 */

namespace Sigosoft\Slider\Controller\Adminhtml\Slides;

class Save extends \Sigosoft\Slider\Controller\Adminhtml\Slides
{
    protected $uploaderFactory;
    protected $imageModel;

    public function __construct(
    	\Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\MediaStorage\Model\File\UploaderFactory $uploaderFactory,
        \Sigosoft\Slider\Model\Slides\Image $imageModel
    )
    {
	$this->uploaderFactory = $uploaderFactory;
	$this->imageModel = $imageModel;
        parent::__construct($context, $coreRegistry, $resultForwardFactory, $resultPageFactory);
    }
    
    public function execute()
    {
        if ($this->getRequest()->getPostValue()) {
            try {
                $model = $this->_objectManager->create('Sigosoft\Slider\Model\Slides');
                $data = $this->getRequest()->getPostValue();
                $inputFilter = new \Zend_Filter_Input(
                    [],
                    [],
                    $data
                );
                $data = $inputFilter->getUnescaped();
                $id = $this->getRequest()->getParam('id');
                if ($id) {
                    $model->load($id);
                    if ($id != $model->getId()) {
                        throw new \Magento\Framework\Exception\LocalizedException(__('The wrong item is specified.'));
                    }
                }
                $model->setData($data);
                $session = $this->_objectManager->get('Magento\Backend\Model\Session');
                $session->setPageData($model->getData());
		$imageName = $this->uploadFileAndGetName('slide', $this->imageModel->getBaseDir(), $data);
				$model->setSlide($imageName);
		$imageName = $this->uploadFileAndGetName('offer', $this->imageModel->getBaseDir(), $data);
                $model->setOfferImage($imageName);
        $imageName = $this->uploadFileAndGetName('mobile', $this->imageModel->getBaseDir(), $data);
            $model->setMobile($imageName);

                //$imageName = $this->uploadFileAndGetName('slide', $this->imageModel->getBaseDir(), $data);
		//$imageName = $this->uploadFileAndGetName('offer', $this->imageModel->getBaseDir(), $data);
		//$model->setSlide($imageName);
                $model->save();
                $this->messageManager->addSuccess(__('You saved the item.'));
                $session->setPageData(false);
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('sigosoft_slider/*/edit', ['id' => $model->getId()]);
                    return;
                }
                $this->_redirect('sigosoft_slider/*/');
                return;
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
                $id = (int)$this->getRequest()->getParam('id');
                if (!empty($id)) {
                    $this->_redirect('sigosoft_slider/*/edit', ['id' => $id]);
                } else {
                    $this->_redirect('sigosoft_slider/*/new');
                }
                return;
            } catch (\Exception $e) {
                $this->messageManager->addError(
                    __('Something went wrong while saving the item data. Please review the error log.')
                );
                $this->_objectManager->get('Psr\Log\LoggerInterface')->critical($e);
                $this->_objectManager->get('Magento\Backend\Model\Session')->setPageData($data);
                $this->_redirect('sigosoft_slider/*/edit', ['id' => $this->getRequest()->getParam('id')]);
                return;
            }
        }
        $this->_redirect('sigosoft_slider/*/');
    }
    public function uploadFileAndGetName($input, $destinationFolder, $data)
    {
    try {
        if (isset($data[$input]['delete'])) {
            return '';
        } else {
            $uploader = $this->uploaderFactory->create(['fileId' => $input]);
            $uploader->setAllowRenameFiles(true);
            $uploader->setFilesDispersion(true);
            $uploader->setAllowCreateFolders(true);
            $result = $uploader->save($destinationFolder);
            return $result['file'];
        }
    } catch (\Exception $e) {
        if ($e->getCode() != \Magento\Framework\File\Uploader::TMP_NAME_EMPTY) {
            throw new FrameworkException($e->getMessage());
        } else {
            if (isset($data[$input]['value'])) {
                return $data[$input]['value'];
            }
        }
    }
    return '';
   }
}
