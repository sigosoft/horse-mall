<?php
/**
 * Copyright © 2015 Sigosoft. All rights reserved.
 */
namespace Sigosoft\Slider\Block\Adminhtml;

class Slides extends \Magento\Backend\Block\Widget\Grid\Container
{
    /**
     * Constructor
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_controller = 'slides';
        $this->_headerText = __('Slides');
        $this->_addButtonLabel = __('Add New Slide');
        parent::_construct();
    }
}
