<?php
namespace Sigosoft\Slider\Block;

use Magento\Framework\View\Element\Template;
use Magento\Framework\ObjectManagerInterface;
 
class Slider extends Template
{
	protected $_storeManager;
	protected $_slidesFactory;
	protected $objectManager;

	public function __construct(
		\Magento\Framework\View\Element\Template\Context $context,
		//\Magento\Store\Model\StoreManagerInterface $storeManager,
		\Sigosoft\Slider\Model\SlidesFactory $slidesFactory,
		ObjectManagerInterface $objectManager
	) {
		//$this->_storeManager = $storeManager;
		$this->_storeManager = $context->getStoreManager();
		$this->_slidesFactory = $slidesFactory;
		$this->objectManager = $objectManager;
		parent::__construct($context);
	}

	public function getSlides()
	{
		$slidesCollection = $this->_slidesFactory->create()->getCollection();
		return $slidesCollection;
	}

	public function getMediaUrl()
	{
		return $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
	}
	public function getLink($slide) {
        $link = "";
        switch($slide->getLinkType()){
            case \Sigosoft\Slider\Model\System\Config\LinkType::NORMAL :
                $link = "";
                break;
            case \Sigosoft\Slider\Model\System\Config\LinkType::EXTERNALLINK :
                $link = $slide->getExternalLink();
                break;
            case \Sigosoft\Slider\Model\System\Config\LinkType::PRODUCT :
                $datatype = $slide->getDataType();
                $productId = basename($datatype);
                $link = $this->getProductLink($productId);
                break;
            case \Sigosoft\Slider\Model\System\Config\LinkType::CATEGORY :
                $datatype = $slide->getDataType();
                $categoryId = basename($datatype);
                $link = $this->getCategoryLink($categoryId);
                break;
            case \Sigosoft\Slider\Model\System\Config\LinkType::BRAND :
                $brandId = $slide->getBrand();
                $link = $this->getBrandLink($brandId);
                break;
        }
        return $link;
    }
    
    public function getProductLink($productId) {
        $product = $this->objectManager->create("Magento\Catalog\Model\Product")
                ->load($productId);
        return $product->getProductUrl();
    }
    
    public function getCategoryLink($categoryId) {
        $category = $this->objectManager->create("Magento\Catalog\Model\Category")
                ->load($categoryId);
        return $category->getUrl();
    }
    
    public function getBrandLink($brandId) {
        return $this->getBaseUrl(). "products.html?manufacturer?id=" . $brandId;
       
    }

}
