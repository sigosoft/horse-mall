var config = {
    paths: {
        'bxslider': 'Sigosoft_Slider/js/jquery.bxslider'
    },
    shim: {
        'bxslider': {
            deps: ['jquery']
        }
    }
};