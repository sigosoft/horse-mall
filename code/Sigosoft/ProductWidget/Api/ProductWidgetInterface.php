<?php
/**
 * Copyright © 2015 Sigosoft. All rights reserved.
 */

namespace Sigosoft\ProductWidget\Api;

interface ProductWidgetInterface
{
	
	/**
	*
	* @api
	* @param string $type
	* @param int $pagefrom
	* @param int $pageto
	* @return $this
	*/
    public function GetWidgetDetails($type,$pagefrom,$pageto);
	
	
}