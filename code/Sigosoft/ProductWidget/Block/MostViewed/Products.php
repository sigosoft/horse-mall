<?php

namespace Sigosoft\ProductWidget\Block\MostViewed;

class Products extends \Magento\Framework\View\Element\Template {

    public function __construct(
    \Magento\Catalog\Block\Product\Context $context, \Magento\Reports\Model\ResourceModel\Product\CollectionFactory $productsFactory, \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory, \Magento\Catalog\Model\Product\Visibility $catalogProductVisibility, \Magento\Framework\Data\Form\FormKey $formkey, \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate, array $data = []
    ) {
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->catalogProductVisibility = $catalogProductVisibility;
        $this->formkey = $formkey;
        $this->localeDate = $localeDate;
        $this->_productsFactory = $productsFactory;
        parent::__construct(
                $context, $data
        );
    }

    protected function _prepareLayout() {
        parent::_prepareLayout();

        if ($this->getProducts()) {
            $pager = $this->getLayout()->createBlock('Magento\Theme\Block\Html\Pager')->setAvailableLimit(array(5 => 5, 10 => 10, 15 => 15, 20 => 20));
            $pager->setLimit(10)->setShowPerPage(true);
            $pager->setCollection($this->getProducts());
            $this->setChild('pager', $pager);
            $this->getProducts()->load();
        }
        return $this;
    }

    public function getProducts() {
        $currentStoreId = $this->_storeManager->getStore()->getId();

        $collection = $this->_productsFactory->create()
                        ->addAttributeToSelect(
                                '*'
                        )->addViewsCount()->setStoreId(
                        $currentStoreId
                )->addStoreFilter(
                $currentStoreId
        );
        $page = ($this->getRequest()->getParam('p')) ? $this->getRequest()->getParam('p') : 1;
        $pageSize = ($this->getRequest()->getParam('limit')) ? $this->getRequest()->getParam('limit') : 10;

        $collection->setPageSize($pageSize);
        $collection->setCurPage($page);
        $collection->setVisibility($this->catalogProductVisibility->getVisibleInCatalogIds());
        return $collection;
    }

    public function getPagerHtml() {
        return $this->getChildHtml('pager');
    }

}
