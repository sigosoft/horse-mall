var config = {
    paths: {
        'carousel': 'Sigosoft_ProductWidget/js/owl.carousel'
    },
    shim: {
        'carousel': {
            deps: ['jquery']
        }
    }
};
